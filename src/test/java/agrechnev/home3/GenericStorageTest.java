package agrechnev.home3;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by User on 8/15/2016.
 */
public class GenericStorageTest {
    @Test
    public void fullTest(){
        GenericStorage<String> gs1=new GenericStorage<>();
        GenericStorage<String> gs2=new GenericStorage<>();

        String[] myData1={"Yuri","Karin","Kato","Gepetto","Blanca","Ouka","Joachim",
                "Lucia","Anastacia","Kurando","Solomon"};
        for (String s: myData1) {
            gs1.add(s);
        }

        // Create gs2 with String objects to avoid String interning
        // in order to test equals properly
        gs2.add(new String("Yuri"));
        gs2.add(new String("Karin"));
        gs2.add(new String("Kato"));
        gs2.add(new String("Gepetto"));
        gs2.add(new String("Blanca"));
        gs2.add(new String("Ouka"));
        gs2.add(new String("Joachim"));
        gs2.add(new String("Lucia"));
        gs2.add(new String("Anastacia"));
        gs2.add(new String("Kurando"));
        gs2.add(new String("Solomon"));

        assertTrue(gs1.equals(gs2)); // equals
        assertEquals(gs1.toString(), //toString
                "GenericStorage{[Yuri, Karin, Kato, Gepetto, Blanca, Ouka,"+
                        " Joachim, Lucia, Anastacia, Kurando, Solomon]}");

        assertEquals(gs1.size(),11); //size
        assertEquals(gs1.get(1),new String("Karin")); //get

        assertEquals(gs1.find(new String("Lucia")),7); // find
        gs1.update(1,"Karin Koenig");
        gs1.delete(2); // delete
        gs1.delete(new String("Ouka"));
        gs1.delete(new String("Solomon"));
        assertEquals(gs1.toString(),//toString
                "GenericStorage{[Yuri, Karin Koenig, Gepetto, Blanca, Joachim, Lucia, Anastacia, Kurando]}");
        assertEquals(gs1.size(),8);
        assertEquals(gs1.find(new String("Lucia")),5); // find

        String[] testArray={"Yuri","Karin Koenig","Gepetto","Blanca","Joachim",
                "Lucia","Anastacia","Kurando"};
        assertArrayEquals(gs1.getAll(),testArray);
    }

    // Test the IndexOutOfBoundsException
    @Test(expected = IndexOutOfBoundsException.class)
    public void boundsTest1(){
        GenericStorage<String> gs=new GenericStorage<>();

        gs.add("String 1");
        gs.add("String 2");

        System.out.println(gs.get(2));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void boundsTest2(){
        GenericStorage<String> gs=new GenericStorage<>();

        gs.add("String 1");
        gs.add("String 2");

        gs.delete(2);
    }
}