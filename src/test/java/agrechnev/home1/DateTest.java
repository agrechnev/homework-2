package agrechnev.home1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test class Date and its Nested Classes
 * Created by Oleksiy Grechnyev on 7/10/2016.
 */
public class DateTest {

  // DayOfWeek
  @Test
  public void testDayOfWeek(){
      for (int i=0;i<=6;i++) { // Run over all days
        try {
            Date.DayOfWeek day = Date.DayOfWeek.getDayofWeek(i); // Day # i

            assertEquals(day.getDayNum(),i);
            assertTrue(Date.DayOfWeek.getDayofWeek(day.toString()) == day);
            assertTrue(Date.DayOfWeek.getDayofWeek(day.toString().toLowerCase()) == day);
        } catch(Date.DateException e) {fail();}
      }
  }

   @Test(expected = Date.DateException.class)
   public void testDayofWeekEx1() throws Date.DateException {
       Date.DayOfWeek day= Date.DayOfWeek.getDayofWeek(18);
   }

   @Test(expected = Date.DateException.class)
   public void testDayofWeekEx2() throws Date.DateException {
       Date.DayOfWeek day= Date.DayOfWeek.getDayofWeek("LALADAY");
   }

    // Day

    @Test
    public void testDay(){
        try {
            for (int i=1;i<=31;i++) {
                assertEquals(new Date.Day(i).getNumber(),i);
            }
        }  catch(Date.DateException e) {fail();}
    }


    @Test(expected = Date.DateException.class)
    public void testDayEx1() throws Date.DateException {
        Date.Day day= new Date.Day(0);
    }

    @Test(expected = Date.DateException.class)
    public void testDayEx2() throws Date.DateException {
        Date.Day day= new Date.Day(32);
    }

    // Month

    @Test
    public void testMonth(){

        // Test getnumber
        for (int i = 1; i <= 12; i++) {
            try {
                assertEquals(new Date.Month(i).getNumber(), i);
            } catch(Date.DateException e) {fail();}
        }
        // Test getDays
        try {
            assertEquals(new Date.Month(1).getDays(false), 31);
            assertEquals(new Date.Month(2).getDays(false), 28);
            assertEquals(new Date.Month(2).getDays(true), 29);
            assertEquals(new Date.Month(3).getDays(false), 31);
            assertEquals(new Date.Month(4).getDays(false), 30);
            assertEquals(new Date.Month(5).getDays(false), 31);
            assertEquals(new Date.Month(6).getDays(false), 30);
            assertEquals(new Date.Month(7).getDays(false), 31);
            assertEquals(new Date.Month(8).getDays(false), 31);
            assertEquals(new Date.Month(9).getDays(false), 30);
            assertEquals(new Date.Month(10).getDays(false), 31);
            assertEquals(new Date.Month(11).getDays(false), 30);
            assertEquals(new Date.Month(12).getDays(false), 31);
        }  catch(Date.DateException e) {fail();}
    }


    @Test(expected = Date.DateException.class)
    public void testMonthEx1() throws Date.DateException {
        Date.Month mon=new Date.Month(0);
    }

    @Test(expected = Date.DateException.class)
    public void testMonthEx2() throws Date.DateException {
        Date.Month mon=new Date.Month(13);
    }

    //Year
    @Test
    public void testYear(){
        assertFalse(new Date.Year(2014).isLeap());
        assertTrue(new Date.Year(2016).isLeap());
        assertFalse(new Date.Year(1900).isLeap());
        assertTrue(new Date.Year(2000).isLeap());
        assertEquals(new Date.Year(2017).getDays(),365);
        assertEquals(new Date.Year(2020).getDays(),366);
    }

    //Date
    @Test
    public void testDate(){
        try {
            // Test the Weekdays
            assertTrue(new Date(31,12,0).getDayOfWeek()== Date.DayOfWeek.SUNDAY);
            assertTrue(new Date(29,2,1248).getDayOfWeek()== Date.DayOfWeek.SATURDAY);
            assertTrue(new Date(11,7,2016).getDayOfWeek()== Date.DayOfWeek.MONDAY);
            assertTrue(new Date(19,11,3044).getDayOfWeek()== Date.DayOfWeek.TUESDAY);

            // Test the Julian days
            assertEquals(new Date(10,7,2016).getJulianDay(),2457580);
            assertEquals(new Date(9,10,1712).getJulianDay(),2346637);
            assertEquals(new Date(7,4,2999).getJulianDay(),2816519);

        } catch (Date.DateException e) {fail();}
    }
}