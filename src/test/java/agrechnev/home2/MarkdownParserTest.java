package agrechnev.home2;

import org.junit.Test;

import static org.junit.Assert.*;
import static agrechnev.home2.MarkdownParser.*;

/**
 * Created by User on 7/31/2016.
 */
public class MarkdownParserTest {
    @Test
    public void testParse(){
        assertNull(parse(null));

        String lineSep=System.lineSeparator(); // The line separator (\n or \r\n)
        String s="##Header line"+ lineSep+
                 "Simple line *with* em" + lineSep+
                 "Simple **line** with strong" + lineSep+
                 "Line with link [Link to google](https://www.google.com) in center" + lineSep+
                 "Line **with** *many* **elements** and link [Link to FB](https://www.facebook.com)";

        String res="<html>"+ lineSep+ lineSep+ "<body>"+ lineSep+ lineSep+
                "<h2>Header line</h2>" + lineSep+ lineSep+
                "<p>Simple line <em>with</em> em</p>" + lineSep+ lineSep+
                "<p>Simple <strong>line</strong> with strong</p>" + lineSep+ lineSep+
                "<p>Line with link <a href=\"https://www.google.com\">Link to google</a> in center</p>"
                + lineSep+ lineSep+
                "<p>Line <strong>with</strong> <em>many</em> <strong>elements</strong> and link "+
                "<a href=\"https://www.facebook.com\">Link to FB</a></p>" + lineSep+ lineSep+
                "</body>"+ lineSep+ lineSep+"</html>"+ lineSep;

        assertEquals(parse(s),res);

    }

}