package agrechnev.home2;

import org.junit.Test;

import static agrechnev.home2.StringUtils.*;
import static org.junit.Assert.*;

/**
 * Created by User on 7/17/2016.
 */
public class StringUtilsTest {
    @Test
    public void reverseTest(){
        String s1="If guns are outlawed, only outlaws will have guns!!!";
        String s1r="!!!snug evah lliw swaltuo ylno ,dewaltuo era snug fI";

        String s2="Український текст із літерами ґ, Ґ, є, Є, ї, Ї.";
        String s2r=".Ї ,ї ,Є ,є ,Ґ ,ґ имаретіл зі тскет йикьснїаркУ";

        String s3="~!@#$%^&*()_+";
        String s3r="+_)(*&^%$#@!~";

        assertEquals(reverse(s1),s1r);
        assertEquals(reverse(s2),s2r);
        assertEquals(reverse(s3),s3r);
        assertNull(reverse(null));

        assertEquals(reverse2(s1),s1r);
        assertEquals(reverse2(s2),s2r);
        assertEquals(reverse2(s3),s3r);
        assertNull(reverse2(null));
    }

    @Test
    public void isPalindromeTest(){
        assertTrue(isPalindrome("Niagara, o roar again !!!"));
        assertTrue(isPalindrome("A man, a plan, a canal - Panama!"));
        assertTrue(isPalindrome("Was it a car or a cat I saw?"));
        assertTrue(isPalindrome("Are we not drawn onward, we few, drawn onward to new era?"));
        assertTrue(isPalindrome("Паліндром — і ні морд, ні лап"));
        assertTrue(isPalindrome("Мило, — Галина до Данила, — голим"));
        assertFalse(isPalindrome("A non-palindrome ..."));
        assertFalse(isPalindrome("фівапро"));
        assertFalse(isPalindrome(null));
    }

    @Test
    public void trim10_6_12Test(){
        assertEquals(trim10_6_12(""),"oooooooooooo");
        assertEquals(trim10_6_12("CAT"),"CATooooooooo");
        assertEquals(trim10_6_12("Palindrome"),"Palindromeoo");
        assertEquals(trim10_6_12("PalindromeX"),"Palind");
        assertEquals(trim10_6_12("Шевченківський район"),"Шевчен");
        assertNull(trim10_6_12(null));
    }

    @Test
    public void swapFirstLastTest(){
        assertEquals(swapFirstLast("1234 Barbros bror badade bara i Barsebäck !!! !@#$%^&*()"),
                "1234 Barsebäck bror badade bara i Barbros !!! !@#$%^&*()"
                );
        assertEquals(swapFirstLast("123"),"123");
        assertEquals(swapFirstLast("Word"),"Word");
        assertEquals(swapFirstLast("123"),"123");
        assertEquals(swapFirstLast(" Two words ?")," words Two ?");
        assertEquals(swapFirstLast("Знаєш, я пекуча, наче крига .."),"крига, я пекуча, наче Знаєш ..");
        assertNull(swapFirstLast(null));
    }

    @Test
    public void swapFirstLastAllTest(){
        assertEquals(swapFirstLastAll("One. Two words. Three-word sentence  ..."),
                "One. words Two. sentence-word Three  ..."
        );
        assertEquals(swapFirstLastAll("One? Two words! Three-word sentence  !"),
                "sentence? Two words! Three-word One  !"
        );
        assertEquals(swapFirstLastAll(""),
                ""
        );
        assertEquals(swapFirstLastAll("# ... % 123 456 *. LA UI ! . ?"),
                "# ... % 123 456 *. UI LA ! . ?"
        );
        assertEquals(swapFirstLastAll(
                "Знаєш, я пекуча, наче крига. Знаєш,я таємна, наче книга. Я тобі чужа і я веду війну."),
                "крига, я пекуча, наче Знаєш. книга,я таємна, наче Знаєш. війну тобі чужа і я веду Я."
        );
        assertNull(swapFirstLastAll(null));
    }

    @Test
    public void isDateTest(){
        assertFalse(isDate(null));
        assertFalse(isDate(""));
        assertFalse(isDate("Abracadabra"));
        assertFalse(isDate("2.3.2016"));
        assertFalse(isDate("02-04-2016"));
        assertFalse(isDate("02 04 2016"));
        assertFalse(isDate(" 02.03.2016"));
        assertFalse(isDate("02 .03.2016"));
        assertFalse(isDate("02.03. 2016"));
        assertFalse(isDate("02.03.2016 "));
        assertFalse(isDate("00.13.2016"));
        assertFalse(isDate("13.13.2016"));
        assertFalse(isDate("05.00.2016"));
        assertFalse(isDate("12.32.2016"));
        assertFalse(isDate("11.31.2016"));
        assertFalse(isDate("02.30.2016"));
        assertFalse(isDate("02.29.2015"));
        assertFalse(isDate("02.29.1900"));
        assertTrue(isDate("02.29.2016"));
        assertTrue(isDate("02.29.2000"));
        assertTrue(isDate("01.01.2000"));
        assertTrue(isDate("12.31.2000"));
    }

    @Test
    public void getPhonesTest(){
        String s1= "1(234)567-89-01   +1[234]567-89-01   +1 (234)567-89-01   +1(234)567 89 01 "+
                "+1(24)567-89-01 +1(234)567-89-012" ;
        String[] ar1={};
        String s2= "+1(234)567-89-01  +1(234)567-89-02  +1(234)567-89-03  phone=+1(234)567-89-04@";
        String[] ar2={"+1(234)567-89-01","+1(234)567-89-02","+1(234)567-89-03","+1(234)567-89-04"};

        assertArrayEquals(getPhones(s1),ar1);
        assertArrayEquals(getPhones(null),ar1);
        assertArrayEquals(getPhones(s2),ar2);
    }

    @Test
    public void isPostAddressTest(){
        assertTrue(isPostAddress("Box 516, 751 20 UPPSALA, Sweden"));
        assertFalse(isPostAddress("Box 516, 751 20"));
        assertFalse(isPostAddress("Box 516,  UPPSALA, Sweden"));
        assertFalse(isPostAddress("516, 751 20 UPPSALA, Sweden"));

        assertTrue(isPostAddress("Prospekt Nauky 47, Kharkiv 61103 , UKRAINE"));
        assertFalse(isPostAddress("Prospekt Nauky 47, Kharkiv 61103"));
        assertFalse(isPostAddress("Prospekt Nauky 4761103 , UKRAINE"));
        assertFalse(isPostAddress("47, Kharkiv 61103 , UKRAINE"));
    }
}