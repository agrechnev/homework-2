package ua.org.oa.agrechnev.task4_1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Oleksiy Grechnyev on 8/22/2016.
 */
public class MyArraysTest {
    @Test
    public void maxElement() throws Exception {
        Integer[] intArray={3,18,4,5,-25,0};

        assertEquals((long)MyArrays.maxElement(intArray),18);

        String[] stringArray={"Tidus","Yuna","Rikku","Lulu","Auron","Wakka","Kimahri"};
        assertEquals(MyArrays.maxElement(stringArray),"Yuna");

    }

}