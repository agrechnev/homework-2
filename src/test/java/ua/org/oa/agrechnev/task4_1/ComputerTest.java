package ua.org.oa.agrechnev.task4_1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Oleksiy Grechnyev on 8/22/2016.
 */
public class ComputerTest {
    @Test
    public  void compareToTest(){
        Computer c1=new Computer("Dell","ALIENWARE 13",2016,2,3100,16384);
        Computer c2=new Computer("Dell","ALIENWARE 13",2016,1,4100,16384);
        Computer c3=new Computer("Dell","ALIENWARE 13",2016,2,2100,26384);
        Computer c4=new Computer("Eell","ALIENWARE 13",2016,2,3100,6384);
        Computer c5=new Computer("Cell","BLIENWARE 13",2016,2,3100,16384);
        Computer c6=new Computer("Dell","AA",2017,2,3100,16384);

        assertTrue("Test Computer.compareTo :",c1.compareTo(c2) > 0);
        assertTrue("Test Computer.compareTo :",c1.compareTo(c3) > 0);
        assertTrue("Test Computer.compareTo :",c1.compareTo(c4) > 0);
        assertTrue("Test Computer.compareTo :",c1.compareTo(c5) > 0);
        assertTrue("Test Computer.compareTo :",c6.compareTo(c1) < 0);
    }
}