package ua.org.oa.agrechnev.task4_2.part3;

import org.junit.Before;
import org.junit.Test;

import java.util.ListIterator;

import static org.junit.Assert.*;

/**
 * Created by Oleksiy Grechnyev on 8/24/2016.
 *  Test the ListIteratorImpl actually
 */
public class MyDequeImplTest {

    private  MyDeque<String> deque;
    private  ListIterator<String> listIterator;
    private static final String[] races ={"Orc","Goblin","Human","Elf","Dwarf","Halfling"};
    private static final String[] races2 ={"Orc","Goblin","Tiefling","Elf","Duergar","Halfling"};
    private static final String[] races3 ={"Orc","Goblin","Dragon","Elf","Halfling"};

    // This is run before every test method
    @Before
    public void setUp() throws Exception {
        // Create a sample list using the ListIterator only
        deque = new MyDequeImpl<>();
        listIterator = deque.listIterator();

        listIterator.add("Orc");
        listIterator.add("Goblin");
        listIterator.add("Dwarf");
        listIterator.add("Halfling");
        listIterator.previous(); // Go 2 steps left (before Dwarf)
        listIterator.previous();
        listIterator.add("Human");
        listIterator.add("Elf");
    }

    @Test
    // Test reading the iterator left and right
    public void test1(){
        // Check the before-result
        assertEquals(listIterator.nextIndex(), 4);
        assertEquals(listIterator.previousIndex(), 3);
        assertEquals(deque.size(), 6);

        // Check MyDeque as array
        assertArrayEquals(deque.toArray(), races);

        // Move left
        assertEquals(listIterator.previous(),"Elf");
        assertEquals(listIterator.previous(),"Human");
        assertEquals(listIterator.previous(),"Goblin");
        assertEquals(listIterator.previous(),"Orc");
        assertFalse(listIterator.hasPrevious());
        assertEquals(listIterator.previousIndex(),-1);

        // Move right
        assertEquals(listIterator.next(),"Orc");
        assertEquals(listIterator.next(),"Goblin");
        assertEquals(listIterator.next(),"Human");
        assertEquals(listIterator.next(),"Elf");
        assertEquals(listIterator.next(),"Dwarf");
        assertEquals(listIterator.next(),"Halfling");
        assertFalse(listIterator.hasNext());
        assertEquals(listIterator.nextIndex(),6);

    }

    @Test(expected = IllegalStateException.class)
    // Remove for a new iterator gives exception
    public void testEx1(){
        listIterator=deque.listIterator();
        listIterator.remove();
    }

    @Test(expected = IllegalStateException.class)
    // Set for a new iterator gives exception
    public void testEx2(){
        listIterator=deque.listIterator();
        listIterator.set("HAHA");
    }


    @Test(expected = IllegalStateException.class)
    // Double remove gives exception
    public void testEx3(){
        listIterator.remove();
        listIterator.remove();
    }

    @Test
    // Remove everything to hell
    public void test2(){
        // Go 4 left
        assertEquals(listIterator.previous(),"Elf");
        listIterator.remove();
        assertEquals(listIterator.previous(),"Human");
        listIterator.remove();
        assertEquals(listIterator.previous(),"Goblin");
        listIterator.remove();
        assertEquals(listIterator.previous(),"Orc");
        listIterator.remove();
        // And 2 right
        assertEquals(listIterator.next(),"Dwarf");
        listIterator.remove();
        assertEquals(listIterator.next(),"Halfling");
        listIterator.remove();
        assertEquals(listIterator.nextIndex(),0);
        assertEquals(deque.size(),0);

        // Now add again to check that it does not break
        listIterator.add("Orc");
        listIterator.add("Goblin");
        listIterator.add("Dwarf");
        listIterator.add("Halfling");
        listIterator.previous(); // Go 2 steps left (before Dwarf)
        listIterator.previous();
        listIterator.add("Human");
        listIterator.add("Elf");


        assertEquals(listIterator.nextIndex(), 4);
        assertEquals(listIterator.previousIndex(), 3);
        assertEquals(deque.size(), 6);

        // Check MyDeque as array
        assertArrayEquals(deque.toArray(), races);
    }

    @Test
    // Change Human to Tiefling and Dwarf to Duergar on the way right
    public void test3(){
        // Move all the way left
        while (listIterator.hasPrevious()) listIterator.previous();

        // Now move right and change
        while (listIterator.hasNext()) {
            switch (listIterator.next()) {
                case "Human":
                    listIterator.set("Tiefling");
                    break;
                case "Dwarf":
                    listIterator.set("Duergar");
                    break;
            }
        }

        assertArrayEquals(deque.toArray(), races2);

    }

    @Test
    // Change Human to Tiefling and Dwarf to Duergar on the way left
    public void test4(){
        // Move all the way right
        while (listIterator.hasNext()) listIterator.next();

        // Now move left and change
        while (listIterator.hasPrevious()) {
            switch (listIterator.previous()) {
                case "Human":
                    listIterator.set("Tiefling");
                    break;
                case "Dwarf":
                    listIterator.set("Duergar");
                    break;
            }
        }

        assertArrayEquals(deque.toArray(), races2);

    }

    @Test
    // Remove Human and Dwarf on the way right, then add Dragon in the middle
    public void test5(){
        // Move all the way left
        while (listIterator.hasPrevious()) listIterator.previous();

        // Now move right and remove
        while (listIterator.hasNext())
            switch (listIterator.next()) {
                case "Human":case "Dwarf":
                    listIterator.remove();
            }

        listIterator.previous();
        listIterator.previous();
        listIterator.add("Dragon");

        assertArrayEquals(deque.toArray(), races3);
    }

    @Test
    // Remove Human and Dwarf on the way left, then add Dragon in the middle
    public void test6(){

        // Move all the way right
        while (listIterator.hasNext()) listIterator.next();

        // Now move left and remove
        while (listIterator.hasPrevious())
            switch (listIterator.previous()) {
                case "Human":case "Dwarf":
                    listIterator.remove();
            }


        listIterator.next();
        listIterator.next();
        listIterator.add("Dragon");

        assertArrayEquals(deque.toArray(), races3);
    }


}