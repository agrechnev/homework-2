package ua.org.oa.agrechnev.task4_2.part1;

import org.junit.Test;

import java.util.NoSuchElementException;

import static org.junit.Assert.*;

/**
 * Created by Oleksiy Grechnyev on 8/22/2016.
 */
public class MyDequeImplTest {

    // Test exception
    @Test(expected = NoSuchElementException.class)
    public void  exceptionTest() throws Exception {
        MyDeque<String> deque=new MyDequeImpl<>();

        deque.getFirst();
    }

    // Test add, remove, size, get methods
    @Test
    public void  fullTest() throws Exception {
        MyDeque<String> deque=new MyDequeImpl<>();

        assertEquals(deque.size(),0);

        // Try adding and getting at both ends

        deque.addFirst("Orc");
        assertEquals(deque.getFirst(),"Orc");
        assertEquals(deque.getLast(),"Orc");
        deque.addFirst("Goblin");
        assertEquals(deque.getFirst(),"Goblin");
        assertEquals(deque.getLast(),"Orc");
        deque.addLast("Zombie");
        assertEquals(deque.getFirst(),"Goblin");
        assertEquals(deque.getLast(),"Zombie");
        deque.addFirst("Ogre");
        assertEquals(deque.getFirst(),"Ogre");
        assertEquals(deque.getLast(),"Zombie");
        deque.addLast("Vampire");
        assertEquals(deque.getFirst(),"Ogre");
        assertEquals(deque.getLast(),"Vampire");


        // Let's clone it = save for later
        MyDeque<String> deque2=new MyDequeImpl<>(deque);

        // And some removing also

        assertEquals(deque.size(),5);
        assertTrue(deque.contains("Ogre"));
        assertFalse(deque.contains("Basilisk"));

        assertEquals(deque.removeFirst(),"Ogre");
        assertEquals(deque.removeLast(),"Vampire");
        assertEquals(deque.size(),3);


        // And adding again

        deque.addFirst("Giant");
        assertEquals(deque.getFirst(),"Giant");
        assertEquals(deque.getLast(),"Zombie");
        deque.addLast("Lich");
        assertEquals(deque.getFirst(),"Giant");
        assertEquals(deque.getLast(),"Lich");

        assertEquals(deque.size(),5);
        assertTrue(deque.contains("Giant"));
        assertTrue(deque.contains("Lich"));
        assertFalse(deque.contains("Ogre")); // Should not exist anymore, we deleted them
        assertFalse(deque.contains("Vampire"));



        // Check the result with removeFirst
        assertEquals(deque.size(),5);
        assertEquals(deque.removeFirst(),"Giant");
        assertEquals(deque.removeFirst(),"Goblin");
        assertEquals(deque.removeFirst(),"Orc");
        assertEquals(deque.removeFirst(),"Zombie");
        assertEquals(deque.removeFirst(),"Lich");
        assertEquals(deque.size(),0);

        // Check the result (older version) with removeLast
        assertEquals(deque2.size(),5);
        assertEquals(deque2.removeLast(),"Vampire");
        assertEquals(deque2.removeLast(),"Zombie");
        assertEquals(deque2.removeLast(),"Orc");
        assertEquals(deque2.removeLast(),"Goblin");
        assertEquals(deque2.removeLast(),"Ogre");
        assertEquals(deque2.size(),0);

    }


    // Tests toArray and clear
    @Test
    public void toArray() throws Exception {
        String[] strings={"Orc","Goblin","Elf","Dwarf"};
        MyDeque<String> deque=new MyDequeImpl<>();

        for (int i = 0; i <4 ; i++) {
            deque.addFirst(strings[3-i]);
        }

        assertEquals(deque.size(),4);
        assertArrayEquals(deque.toArray(),strings);


        deque.clear();
        assertEquals(deque.size(),0);


        for (int i = 0; i <4 ; i++) {
            deque.addLast(strings[i]);
        }
        assertEquals(deque.size(),4);

        assertArrayEquals(deque.toArray(),strings);

    }

    // Test containsAll and the clone constructor
    @Test
    public void containsAll() throws Exception {
        String[] strings={"Orc","Goblin","Elf","Dwarf"};
        MyDeque<String> deque=new MyDequeImpl<>();
        for (int i = 0; i <4 ; i++) {
            deque.addLast(strings[i]);
        }

        // Copy the deque
        MyDeque<String> deque2=new MyDequeImpl<>(deque);

        assertEquals(deque.size(),4);
        assertEquals(deque2.size(),4);

        assertTrue(deque.containsAll(deque2));
        deque.removeFirst();
        assertEquals(deque.size(),3);

        assertFalse(deque.containsAll(deque2));
        deque.clear();
        assertEquals(deque.size(),0);

        assertFalse(deque.containsAll(deque2));

    }

}