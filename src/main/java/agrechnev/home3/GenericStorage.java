package agrechnev.home3;

import java.util.Arrays;

/**
 * Created by Oleksiy Grechnyev on 8/15/2016.
 * Home assignment 3: Generic storage
 * This is basically ArrayList, Light Version
 * Type erasure of generics means that I have to use type Object
 * for the array
 */
public class GenericStorage<T> {

    // Default capacity of the array
    private static final int DEFAULT_CAPACITY=10;
    // Increment of the array size when expanding
    private static final int CAPACITY_INCREMENT=10;

    // Current size of the list (not array)
    // Also the position of the first empty element
    private int size;

    // The data as array
    private Object[] data;

    // Constructor with capacity
    public GenericStorage(int capacity) {
        this.size = 0;
        data=new Object[capacity];
    }

    // Constructor w/o size
    public GenericStorage() {
        this.size = 0;
        data=new Object[DEFAULT_CAPACITY];
    }
    //Check the capacity of the array and expand it if needed
    private void checkCapacity(int newSize){
        if (newSize>data.length) {
            data= Arrays.copyOf(data,data.length+CAPACITY_INCREMENT);
        }
    }
    //Add an element
    public void add(T obj){
        checkCapacity(size+1);
        data[size++]=obj;
    }
    //Get an element with index=0,..,size-1
    public T get(int index){
        if (index<0 || index>=size) {
            throw new IndexOutOfBoundsException("Index out of bounds in GenericStorage.get(int index)");
        } else {
            return (T)data[index];
        }
    }
    // Find the first occurrence of an element
    // returns -1 if not found
    public int find(T obj){
        // Search the entire array
        for (int i = 0; i < size; i++) {
            if (obj.equals(data[i]))   { // Found !
                return i;
            }
        }
        return -1; // Not found
    }

    // Get the entire array as a trimmed copy
    public T[] getAll(){
        return (T[])Arrays.copyOf(data,size);
    }

    // Delete an element by index
    public boolean delete(int index){
        if (index<0 || index>=size) {
            throw new IndexOutOfBoundsException("Index out of bounds in GenericStorage.delete(int index)");
        } else {
            int nMoved=size-index-1; // Number of elements moved

            // Move the data
            if (nMoved>0) System.arraycopy(data,index+1,data,index,nMoved);

            data[--size]=null; // Nullify the unused element and decrease size by 1
            return true;
        }
    }
    // Delete the first occurrence of a given object
    public boolean delete(T obj){
        // Search the entire array
        for (int i = 0; i < size; i++) {
            if (obj.equals(data[i]))   { // Found !
                return delete(i);
            }
        }
        return false; // Not found
    }
    // Trim the array to size
    public void trim(){
        if (size==0) { // Empty array
            data=new Object[]{};
        } else if (size<data.length) {
            data= Arrays.copyOf(data,size); // Resize the array
        }
    }
    // Return the size
    public int size(){ return size;}
    // Update an element
    public boolean update(int index, T obj){
        if (index<0 || index>=size) {
            return false; //Index out of range
        } else {
            data[index]=obj;
            return true;
        }
    }

    // Compare the two lists


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        // null or wrong class
        if (o == null || getClass() != o.getClass()) return false;

        GenericStorage<?> that = (GenericStorage<?>) o;

        if (size != that.size) return false; // Size mismatch

        return Arrays.equals(getAll(), that.getAll());

    }

    @Override
    public int hashCode() {
        int result = size;
        result = 31 * result + Arrays.hashCode(getAll());
        return result;
    }

    //Convert the list to String
    @Override
    public String toString() {
        return "GenericStorage{"+ Arrays.toString(getAll()) + '}';
    }

    // A simple main to test this class
    public static void main(String[] args) {
        GenericStorage<String> gs1=new GenericStorage<>();
        GenericStorage<String> gs2=new GenericStorage<>();

        String[] myData1={"Yuri","Karin","Kato","Gepetto","Blanca","Ouka","Joachim",
                "Lucia","Anastacia","Kurando","Solomon"};
        for (String s: myData1) {
            gs1.add(s);
        }

        // Create gs2 with String objects to avoid String interning
        // in order to test equals properly

        gs2.add(new String("Yuri"));
        gs2.add(new String("Karin"));
        gs2.add(new String("Kato"));
        gs2.add(new String("Gepetto"));
        gs2.add(new String("Blanca"));
        gs2.add(new String("Ouka"));
        gs2.add(new String("Joachim"));
        gs2.add(new String("Lucia"));
        gs2.add(new String("Anastacia"));
        gs2.add(new String("Kurando"));
        gs2.add(new String("Solomon"));

        System.out.println(gs1.equals(gs2)); // equals
        System.out.println(gs1); //toString
        System.out.println(gs1.size()); //size
        System.out.println(gs1.get(1)); //get

        System.out.println(gs1.find(new String("Lucia"))); // find
        gs1.update(1,"Karin Koenig");
        gs1.delete(2); // delete
        gs1.delete(new String("Ouka"));
        gs1.delete(new String("Solomon"));
        System.out.println(gs1); //toString
        System.out.println(gs1.size()); //toString
        System.out.println(gs1.find(new String("Lucia"))); // find

        // Generates IndexOutOfBoundsException
//        gs1.get(8);
//        gs1.get(-1);
//        gs1.delete(8);
//        gs1.delete(-1);


    }
}
