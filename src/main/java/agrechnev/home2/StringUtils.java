package agrechnev.home2;

import java.util.ArrayList;
import java.util.regex.*;

/**
 * Created by Alex Grechnev on 7/17/2016.
 */
public class StringUtils {

    //---------------------------
    // Reverse a string: 2 versions
    public static String reverse(String s){
        if (s==null) return null;
        return new StringBuilder(s).reverse().toString();
    }

    public static String reverse2(String s){
        if (s==null) return null;
        int l=s.length();
        char[] arr=new char[l];

        for (int i = 0; i < l; i++) {
           arr[i]=s.charAt(l-i-1);
        }

        return new String(arr);
    }

    //---------------------------
    // Check if it's a palindrome
    public static boolean isPalindrome(String s){

        if (s==null) return false;

        // Remove junk
        // Note: \w would not work with Cyrillic and other alphabets
        String s1=s.replaceAll("[^\\p{IsAlphabetic}]","").toUpperCase();

//        System.out.println(s1);

        if (s1.length() == 0) {
            return false;
        } else {
            return s1.equals(reverse(s1));
        }
    }

    //---------------------------
    // The funny 10-6-12 operation
    public static String trim10_6_12(String s){
        if (s==null) return null;
        int l=s.length();
        if (l>10) {
            return s.substring(0,6);
        } else {
            return s.concat("oooooooooooo".substring(l));
        }
    }


    //---------------------------
    // Swap the first and last word
    public static String swapFirstLast(String s){
        /* Note:
         Greedy matching=important
         \p{IsAlphabetic} includes cyrillic, greek alphabets etc
         It does NOT include numbers 0-9 !
         []s can be skipped here, but i looks better with them
         Idea: 2 words separated by word boundaries and anything in between (including words)
        */

        if (s==null) return null;
        return s.replaceAll("([\\p{IsAlphabetic}]+)\\b(.+)\\b([\\p{IsAlphabetic}]+)","$3$2$1");
    }

    //---------------------------
    // Swap the first and last word in every sentence
    // Sentences are separated by period ('.') and nothing else
    public static String swapFirstLastAll(String s){
        /* Idea: just like previous routine
         But now the middle section must not contain period ('.'), done by ([^\.]+)
          thus sentences are processed separately despite the greedy match
         */
        if (s==null) return null;

        return s.replaceAll("([\\p{IsAlphabetic}]+)\\b([^\\.]+)\\b([\\p{IsAlphabetic}]+)","$3$2$1");
    }

    //---------------------------
    // Check if s is a date in MM.DD.YYYY format
    public static boolean isDate(String s){
        if (s==null) return false;

        String regex="(\\d{2})\\.(\\d{2})\\.(\\d{4})";
        Pattern p=Pattern.compile(regex);
        Matcher m=p.matcher(s);

        if (!m.matches()) return false;

        int month=Integer.parseInt(m.group(1));
        int day=Integer.parseInt(m.group(2));
        int year=Integer.parseInt(m.group(3));

        return isDateCorrect(month,day,year); // Check the date
    }

    //---------------------------
    // Check if s is a date in correct
    private static boolean isDateCorrect(int month, int day, int year){
        if (month<1 || month>12) return false;

        boolean leap;
        // Find out is a year is leap year
        if (year % 4 != 0) {
            leap=false;
        } else if (year % 100 !=0) {
            leap=true;
        } else if (year % 400 !=0) {
            leap=false;
        } else {
            leap=true;
        }

        // Number of days in the month
        int nd;
        switch (month) {
            case 1: nd=31;
                break;
            case 2: nd= leap ? 29 : 28;
                break;
            case 3: nd=31;
                break;
            case 4: nd=30;
                break;
            case 5: nd=31;
                break;
            case 6: nd=30;
                break;
            case 7: nd=31;
                break;
            case 8: nd=31;
                break;
            case 9: nd=30;
                break;
            case 10: nd=31;
                break;
            case 11: nd=30;
                break;
            case 12: nd=31;
                break;
            default:
                throw new Error("This cannot be !");
        }

        return day>=1 && day<=nd;
    }

    //---------------------------
    // Return all +Х(ХХХ)ХХХ-ХХ-ХХ telephone numbers as a String[] array
    public static String[] getPhones(String s) {

        if (s==null) return new String[]{};

        String regex="\\+\\d\\(\\d{3}\\)\\d{3}\\-\\d{2}\\-\\d{2}\\b";
        ArrayList<String> list=new ArrayList<>();
        Matcher m=Pattern.compile(regex).matcher(s);
        while (m.find()){
            list.add(m.group());
        }

        return list.toArray(new String[]{});

    }

    //---------------------------
    /*Check if the string is a post address
      Not so easy, how do we define a post address?
      Examples:
      Prospekt Nauky 47, Kharkiv 61103 , UKRAINE
      Box 516, 751 20 UPPSALA, Sweden

      So we assume the structure:
      <word> <number> <number> <word> with anything in between
    */
    public static boolean isPostAddress(String s){
        return s.matches(".*\\p{IsAlphabetic}+.*\\d+\\D+\\d+.*\\p{IsAlphabetic}+.*");
    }
}
