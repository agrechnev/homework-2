package agrechnev.home2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alex Grechnev on 7/31/2016.
 */
public class MarkdownParser {
    public static String parse(String s){

        if (s == null) {
            return null;
        }

        String lineSep=System.lineSeparator(); // The line separator (\n or \r\n)

        // Fisrt split s into lines
        String[] sarr=s.split("[\\r\\n]+"); // Includes Dos and Unix newlines

        // Our result, I'll add line by line, start with the header
        StringBuilder result=new StringBuilder("<html>");
        result.append(lineSep);
        result.append(lineSep);
        result.append("<body>");
        result.append(lineSep);
        result.append(lineSep);

        String lineHTML; // The line processed into HTML
        int hCount; // Number of #'s in the beginning of line or 0
        String lineMD2; // The MD line without #'s
        String lineHTML2; // The processed lineMD2

        //--------
        for (String lineMD: sarr) {
            // s.split() can in principle create empty strings, let's ignore them
            // Shouldn't ever happen with the regex I used, but let's be safe
            if (lineMD.length()>0) {
                // Now I process a single line s1
                // First, let's count the #'s
                Matcher m= Pattern.compile("(#*)(.*)").matcher(lineMD);
                if (!m.matches()) throw new Error("BAD error in  MarkdownParser.parse !");
                hCount=m.group(1).length();
                lineMD2=m.group(2); // The remaining line w/o #'s

                lineHTML2=parseLine(lineMD2);
                if (hCount > 0) { // Header line
                    lineHTML=String.format("<h%d>%s</h%d>",hCount,lineHTML2,hCount);
                } else {
                    lineHTML=String.format("<p>%s</p>",lineHTML2);
                }

                result.append(lineHTML);
                result.append(lineSep); // Don't forget to put the line separators back. Twice.
                result.append(lineSep);
            }
        }

        // Now add the footer
        result.append("</body>");
        result.append(lineSep);
        result.append(lineSep);
        result.append("</html>");
        result.append(lineSep);

        return result.toString();
    }

    // Parse one line making em, strong and links,
    // But no ##'s, this is done elsewhere
    private static String parseLine(String s){
        if (s == null) {
            return null;
        }

        // Process strongs
        // This regex would NOT work if there are ems inside of strongs,
        // But it's not allowed by definition of the problem
        String s1=s.replaceAll("(\\*\\*)([^\\*]++)(\\*\\*)","<strong>$2</strong>");

        // Process ems
        String s2=s1.replaceAll("(\\*)([^\\*]++)(\\*)","<em>$2</em>");

        // Process links
        String s3=s2.replaceAll("(\\[)([^\\[\\]\\(\\)]+)(\\]\\()([^\\[\\]\\(\\)]+)(\\))",
                "<a href=\"$4\">$2</a>");

        return s3;
    }

    public static void main(String[] args) { // A sample run of parse
        String lineSep=System.lineSeparator();

        String s="##Header line"+ lineSep+
                "Simple line *with* em" + lineSep+
                "Simple **line** with strong" + lineSep+
                "Line with link [Link to google](https://www.google.com) in center" + lineSep+
                "Line **with** *many* **elements** and link [Link to FB](https://www.facebook.com)";

        System.out.println(parse(s));
    }
}
