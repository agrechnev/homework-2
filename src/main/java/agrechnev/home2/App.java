package agrechnev.home2;

import java.util.Scanner;
import static agrechnev.home2.StringUtils.*;


/**
 * Created by User on 7/17/2016.
 */
public class App {
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);

        System.out.println("Enter a string:");
        String s=in.nextLine();

        System.out.println("reverse(s)="+reverse(s));
        System.out.println("reverse2(s)="+reverse2(s));
        System.out.println("isPalindrome(s)="+isPalindrome(s));
        System.out.println("trim10_6_12(s)="+trim10_6_12(s));
        System.out.println("swapFirstLast(s)="+swapFirstLast(s));
        System.out.println("swapFirstLastAll(s)="+swapFirstLastAll(s));
        System.out.println("isDate(s)="+isDate(s));

        System.out.println("getPhones(s)=");
        for (String s1: getPhones(s)) {
            System.out.println(s1);
        }

        System.out.println("isPostAddress(s)="+isPostAddress(s));
    }
}
