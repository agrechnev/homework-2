package agrechnev.home1;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Date.DateException {
	    Scanner in=new Scanner(System.in);

        int dd,mm,yy;

        System.out.println("Enter date1: DD MM YYYY");
        dd=in.nextInt();
        mm=in.nextInt();
        yy=in.nextInt();

        Date date1=new Date(dd,mm,yy);
        System.out.println("DayOfYear = "+date1.getDayOfYear());
        System.out.println("Week day = "+date1.getDayOfWeek());
        System.out.println("Julian day = "+date1.getJulianDay());

        System.out.println("Enter date2: DD MM YYYY");
        dd=in.nextInt();
        mm=in.nextInt();
        yy=in.nextInt();

        Date date2=new Date(dd,mm,yy);
        System.out.println("DayOfYear = "+date2.getDayOfYear());
        System.out.println("Week day = "+date2.getDayOfWeek());
        System.out.println("Julian day = "+date2.getJulianDay());

        System.out.println("-----");
        System.out.println("Days between = "+date1.daysBetween(date2));
    }
}
