package agrechnev.home1;


// Class Car demo with anonymous classes
// Alex Grechnev
public class MainCar {

    public static void main(String[] args) {
        Car car1=new Car("Tesla Model Y",2020,"SAURONRULES") {
            @Override
            public String toString() {
                return "HAPPY "+super.toString();
            }

            @Override
            public boolean equals(Object o) {
                System.out.println("A very HAPPY equals!");
                return super.equals(o);
            }
        };

        Car car2=new Car("Nissan Leaf",2014,"IAMLORDVOLDEMORT"){
            @Override
            public String toString() {
                return "SAD "+super.toString();
            }

            @Override
            public boolean equals(Object o) {
                System.out.println("A very SAD equals!");

                return super.equals(o);
            }
        };

        Car car3=new Car("Tesla Model Y",2020,"SAURONRULES");

        Car car4=new Car("Nissan Leaf",2014,"IAMLORDVOLDEMORT");

        System.out.println("car1="+car1);
        System.out.println("car2="+car2);
        System.out.println("car3="+car3);
        System.out.println("car4="+car4);

        System.out.println(car1.equals(car3));
        System.out.println(car3.equals(car1));
        System.out.println(car2.equals(car4));
        System.out.println(car4.equals(car2));

        System.out.println(car1.equals(car2));
        System.out.println(car2.equals(car1));

    }
}
