package agrechnev.home1;

/**
 * Created by User on 7/11/2016.
 */
public class Car {
    private String modelName;
    private int modelYear;
    private String registrationPlate;

    public Car(String modelName, int modelYear, String registrationPlate) {
        this.modelName = modelName;
        this.modelYear = modelYear;
        this.registrationPlate = registrationPlate;
    }

    public String getModelName() {
        return modelName;
    }

    public int getModelYear() {
        return modelYear;
    }

    public String getRegistrationPlate() {
        return registrationPlate;
    }

    @Override
    public String toString() {
        return "Car{" +
                "modelName='" + modelName + '\'' +
                ", modelYear=" + modelYear +
                ", registrationPlate='" + registrationPlate + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;

        Car car = (Car) o;

        if (modelYear != car.modelYear) return false;
        if (modelName != null ? !modelName.equals(car.modelName) : car.modelName != null) return false;
        return registrationPlate != null ? registrationPlate.equals(car.registrationPlate) : car.registrationPlate == null;

    }

    }
