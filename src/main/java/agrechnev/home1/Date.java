package agrechnev.home1;

/**
 * Created by Alex Grechnev on 7/10/2016.
 Home assignment #1:
  Date (DD-MM-YYYY) as a class
  Strictly Gregorian calendar is used
 */


public class Date {

    // Exception for class Date
    public static class DateException extends Exception {
        DateException(String s) {super(s);}
    }




    //-------------------

    // Day of week
    public static enum DayOfWeek{
      MONDAY(0),TUESDAY(1),WEDNESDAY(2),THURSDAY(3),FRIDAY(4),SATURDAY(5),SUNDAY(6);


        private final int dayNum;

        DayOfWeek(int dayNum) {this.dayNum=dayNum;} // Constructor

        public int getDayNum() {
            return dayNum;
        }

        // DayOfWeek from int
        public static DayOfWeek getDayofWeek(int day) throws DateException{

            for (DayOfWeek d: DayOfWeek.values()) {
               if (d.getDayNum()==day) return d;
            }
            throw new DateException("DayOfWeek: Wrong day of week in getDayofWeek(int) !");
        }

        // DayofWeek from String
        public static DayOfWeek getDayofWeek(String s) throws DateException{
            try {
                return DayOfWeek.valueOf(s.toUpperCase());
            }
            catch (IllegalArgumentException e) {
                throw new DateException("DayofWeek: Wrong day of week in getDayofWeek(String) !");
            }
        }
    }

    //-------------------
    // Day of Month: 1-31, no checks for shorter Months, rather trivial
    public static class Day{
        private int number;


        public Day(int number)  throws DateException {
            setNumber(number);
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number)  throws DateException{
            if (number < 1 || number > 31) {
                throw new DateException("Day: Wrong day in setNumber/constructor !");
            } else {
                this.number = number;
            }

        }

    }

    //-------------------
    // Month
    public static class Month{

        private int number; // Month number

        public Month(int number) throws DateException{
            setNumber(number);
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number)  throws DateException{
            if (number < 1 || number > 12) {
                throw new DateException("Month: Wrong month in setNumber/constructor !");
            } else {
                this.number = number;
            }
        }

        // Number of days in this month
        public int getDays(boolean leapYear){
            int nd; // Number of days

            switch (number) {
                case 1: nd=31;
                        break;
                case 2: nd= leapYear ? 29 : 28;
                    break;
                case 3: nd=31;
                    break;
                case 4: nd=30;
                    break;
                case 5: nd=31;
                    break;
                case 6: nd=30;
                    break;
                case 7: nd=31;
                    break;
                case 8: nd=31;
                    break;
                case 9: nd=30;
                    break;
                case 10: nd=31;
                    break;
                case 11: nd=30;
                    break;
                case 12: nd=31;
                    break;
                default:
                    throw new Error("Month: This cannot be !");
            }
            return nd;
        }
    }

    //-------------------
    // Year: now things are getting interesting!
    // Note: must use negative years for BC
    // e.g. year  -9 = year 10 BC
    // We use int, not long, so Year is limited by ~ +- 2 billion years
    // No Big Bang !


    public static class Year{
        private int number;
        private boolean leap;

        public Year(int number) {
            setNumber(number);
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;

            // Now check if it's a leap year
            if (number % 4 != 0) {
                leap=false;
            } else if (number % 100 !=0) {
                leap=true;
            } else if (number % 400 !=0) {
                leap=false;
            } else {
                leap=true;
            }
        }

        public boolean isLeap() {
            return leap;
        }

        // Number of days in this year
        public int getDays() {
            return leap ? 366 : 365;
        }
    }


    //-----------------------------------------------
    // End of nested clasees
    // ----------------------------------------------
    // Start of Date proper
    // Some of the code here is clumsy, but it should work
    // I did NOT use java.time API, of course
    // ----------------------------------------------


    Day day;
    Month month;
    Year year;

    // Constructors
    public Date(Day day, Month month, Year year)  throws DateException{
        setDate(day,month,year);
    }

    public Date(int day, int month, int year)  throws DateException{
        setDate(new Day(day),new Month(month),new Year(year));
    }
    //---

    public Day getDay() {
        return day;
    }

    public Month getMonth() {
        return month;
    }

    public Year getYear() {
        return year;
    }

    public void setDate(Day day, Month month, Year year) throws DateException{
        this.year = year;
        this.month = month;
        if (day.getNumber()>month.getDays(year.isLeap())) {
            throw new DateException("Date: Wrong day in setDate !");
        }
        this.day = day;
    }
    //---

    // Get the day of year, Jan 1 = 1
    public int getDayOfYear(){
        int doy=day.getNumber();

        for (int mon=1;mon<month.getNumber();mon++) {
            try {
                doy += new Month(mon).getDays(year.isLeap());
            } catch (DateException e) {
                throw new Error("Impossible !");
            }
        }
        return doy;
    }

    //---
    // Get the day of week
    // Algorithm from:
    // http://www.tondering.dk/claus/cal/chrweek.php


    public DayOfWeek getDayOfWeek(){
        int year=this.year.getNumber();
        int month=this.month.getNumber();
        int day=this.day.getNumber();

        int a=div(14-month,12);
        int y=year-a;
        int m=month+12*a-2;

        // Day of week from the formula
        // 0=SUN, 1=MON, ..., 6=SAT

        int d=mod(day + y + div(y,4) - div(y,100) + div(y,400) + div(31*m,12),7);

        // Convert to our days 0=MON, .., 6=SUN
        d--;
        if (d==-1) d=6;

        try {
            return DayOfWeek.getDayofWeek(d);
        } catch (DateException e) {
            throw new Error("OH NO!");
        }
    }

    // Get the Julian day from date
    // https://en.wikipedia.org/wiki/Julian_day#Calculation
    // Note the use of long, it is important
    // (and would suffice if Year is int)

    public long getJulianDay() {
        long year = this.year.getNumber();
        long month = this.month.getNumber();
        long day = this.day.getNumber();

        long a = ldiv(14 - month, 12);
        long y = year +4800- a;
        long m = month + 12 * a - 3;

        return day + ldiv(153*m+2,5)+365*y+ldiv(y,4) -ldiv(y,100)+ldiv(y,400) - 32045 ;
    }

    // Get days between two dates
    public long daysBetween(Date date) {
        return date.getJulianDay()-getJulianDay();
    }


    //---
    // The floor integer mod  and div operators, we might need it e.g. for BC years
    // Always x=div(x,y)*y+mod(x,y)
    // E.g. div(-3,10)=-1, mod(-3,10)=7

    private static int mod(int x, int y)
    {
        int result = x % y;
        return result < 0? result + y : result;
    }

    private static long lmod(long x, long y)
    {
        long result = x % y;
        return result < 0? result + y : result;
    }

    private static int div(int x, int y)
    {
        int result = x / y;
        return x%y < 0? result -1 : result;
    }

    private static long ldiv(long x, long y)
    {
        long result = x / y;
        return x%y < 0? result -1 : result;
    }
}
