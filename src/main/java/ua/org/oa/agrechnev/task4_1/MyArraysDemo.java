package ua.org.oa.agrechnev.task4_1;

import static ua.org.oa.agrechnev.task4_1.MyArrays.maxElement;

/**
 * Created by Oleksiy Grechnyev on 8/18/2016.
 */
public class MyArraysDemo {
    public static void main(String[] args) {
        System.out.println("A little demo of MyArrays.maxElement :");
        System.out.println("--------Integer array----------");
        Integer[] integerArray={9,3,-78,10,7,2,1};

        System.out.println("MAX="+maxElement(integerArray));

        System.out.println("--------String array----------");
        String[] stringArray={"Tidus","Auron","Rikku","Wakka","Lulu","Yuna","Kimahri"};
        System.out.println("MAX="+maxElement(stringArray));

        System.out.println("--------Computer array----------");
        Computer[] computerArray={
            new Computer("Dell","ALIENWARE 13",2016,2,3100,16384),
            new Computer("Acer","EX2519-P2H5",2015,4,1600,4096),
            new Computer("Lenovo","IdeaPad 100",2016,4,2600,4096),
            new Computer("Apple","A1278 MacBook Pro",2016,2,2500,4096),
        };

        System.out.println("MAX="+maxElement(computerArray));

        System.out.println("--------Car array----------");
        Car[] carArray={
                new Car("X-Wing",1000,"DEATHTOSITH"),
                new Car("Tesla Model Y",2020,"SAURONRULES"),
                new Car("Nissan Leaf",2014,"IAMLORDVOLDEMORT")
        };

        // Syntax error !
        // System.out.println("MAX="+maxElement(carArray));
    }
}
