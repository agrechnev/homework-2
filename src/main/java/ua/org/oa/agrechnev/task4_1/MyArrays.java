package ua.org.oa.agrechnev.task4_1;

/**
 * Created by Oleksiy Grechnyev on 8/18/2016.
 * Contains the single maxElement method
 * To find the Maximum element in an array
 */
public class MyArrays {
    // Suppresses default constructor, ensuring non-instantiability.
    private MyArrays() {}

    // Find a maximum element in an array
    // Element type must implement Comparable
    // Throws IllegalArgumentException if the array is null or empty
    public static <T extends Comparable<T>> T maxElement(T[] array) throws IllegalArgumentException{
        if (array == null || array.length==0) {
            throw new IllegalArgumentException(
                    "Argument array is null or empty in MyArrays.maxElement(T[] array)!");
        }

        T best=array[0]; // The largest element found so far

        for (int i = 1; i < array.length; i++) { // Note that we skip i==0
            if (array[i].compareTo(best) >0) {
                best=array[i]; // Find a new best
            }
        }

        return best;
    }
}
