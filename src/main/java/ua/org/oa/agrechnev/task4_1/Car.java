package ua.org.oa.agrechnev.task4_1;

/**
 * Created by Oleksiy Grechnyev on 8/18/2016.
 * A simple class for a car
 * Note: does NOT implement Comparable
 */
public class Car {
    private String modelName;
    private int modelYear;
    private String registrationPlate;


    // Constructors
    public Car() {
    }

    public Car(String modelName, int modelYear, String registrationPlate) {
        this.modelName = modelName;
        this.modelYear = modelYear;
        this.registrationPlate = registrationPlate;
    }

    public String getModelName() {
        return modelName;
    }

    public int getModelYear() {
        return modelYear;
    }

    public String getRegistrationPlate() {
        return registrationPlate;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public void setModelYear(int modelYear) {
        this.modelYear = modelYear;
    }

    public void setRegistrationPlate(String registrationPlate) {
        this.registrationPlate = registrationPlate;
    }

    @Override
    public String toString() {
        return "Car{" +
                "modelName='" + modelName + '\'' +
                ", modelYear=" + modelYear +
                ", registrationPlate='" + registrationPlate + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;

        Car car = (Car) o;

        if (modelYear != car.modelYear) return false;
        if (modelName != null ? !modelName.equals(car.modelName) : car.modelName != null) return false;
        return registrationPlate != null ? registrationPlate.equals(car.registrationPlate) : car.registrationPlate == null;

    }

    @Override
    public int hashCode() {
        int result = modelName != null ? modelName.hashCode() : 0;
        result = 31 * result + modelYear;
        result = 31 * result + (registrationPlate != null ? registrationPlate.hashCode() : 0);
        return result;
    }
}
