package ua.org.oa.agrechnev.task4_1;

/**
 * Created by Oleksiy Grechnyev on 8/18/2016.
 */
public class Computer implements Comparable<Computer> {

    // Fields
    private String manufacturer;
    private String modelName;
    private int modelYear;

    private int numCores; // Number of cores
    private int clockFrequency; // In MHz
    private int ram;  // In Mb

    // Constructors
    public Computer() {
    }

    public Computer(String manufacturer, String modelName, int modelYear, int numCores, int clockFrequency, int ram) {
        this.manufacturer = manufacturer;
        this.modelName = modelName;
        this.modelYear = modelYear;
        this.numCores = numCores;
        this.clockFrequency = clockFrequency;
        this.ram = ram;
    }

    // Getters+Setters
    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public int getModelYear() {
        return modelYear;
    }

    public void setModelYear(int modelYear) {
        this.modelYear = modelYear;
    }

    public int getNumCores() {
        return numCores;
    }

    public void setNumCores(int numCores) {
        this.numCores = numCores;
    }

    public int getClockFrequency() {
        return clockFrequency;
    }

    public void setClockFrequency(int clockFrequency) {
        this.clockFrequency = clockFrequency;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    // equals + hashCode
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Computer computer = (Computer) o;

        if (modelYear != computer.modelYear) return false;
        if (numCores != computer.numCores) return false;
        if (clockFrequency != computer.clockFrequency) return false;
        if (ram != computer.ram) return false;
        if (manufacturer != null ? !manufacturer.equals(computer.manufacturer) : computer.manufacturer != null)
            return false;
        return modelName != null ? modelName.equals(computer.modelName) : computer.modelName == null;

    }

    @Override
    public int hashCode() {
        int result = manufacturer != null ? manufacturer.hashCode() : 0;
        result = 31 * result + (modelName != null ? modelName.hashCode() : 0);
        result = 31 * result + modelYear;
        result = 31 * result + numCores;
        result = 31 * result + clockFrequency;
        result = 31 * result + ram;
        return result;
    }

    // toString

    @Override
    public String toString() {
        return "Computer{" +
                "manufacturer='" + manufacturer + '\'' +
                ", modelName='" + modelName + '\'' +
                ", modelYear=" + modelYear +
                ", numCores=" + numCores +
                ", clockFrequency=" + clockFrequency +
                ", ram=" + ram +
                '}';
    }


    // Implements compareTo from Comparable
    // Priorities:
    // numCores, clockFrequency, ram, manufacturer, modelName, modelYear

    @Override
    public int compareTo(Computer o) throws NullPointerException{
        int tmp;

        // Check for null Strings and null o
        if (o==null|| manufacturer == null || modelName==null || o.manufacturer == null || o.modelName==null) {
            throw new NullPointerException("Null reference in Computer.compareTo !");
        }

        // Compare down the priority list

        tmp=Integer.compare(numCores,o.numCores);
        if (tmp !=0) return tmp;

        tmp=Integer.compare(clockFrequency,o.clockFrequency);
        if (tmp !=0) return tmp;

        tmp=Integer.compare(ram,o.ram);
        if (tmp !=0) return tmp;

        tmp=manufacturer.compareTo(o.manufacturer);
        if (tmp !=0) return tmp;

        tmp=modelName.compareTo(o.modelName);
        if (tmp !=0) return tmp;

        return Integer.compare(modelYear,o.modelYear);

    }
}
