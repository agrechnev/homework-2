package ua.org.oa.agrechnev.task5;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Oleksiy Grechnyev on 8/28/2016.
 * Mini-translator (translate text files with a dictionary from a file)
 */
public class Tran {

    private Tran() {throw new Error();} // No instantiation

    private static class DictionaryLoadException extends Exception {
        DictionaryLoadException(String message) {
            super(message);
        }
    }

    // Global fields
    private static File dicDir; // Directory with dictionaries
    private static String dictStr; // Dictionary currently loaded  as a string (e.g. "en ua") or null
    private static Map<String,String> dict; // The dictionary currently loaded or null


    // Translate the data from a text file using the dict
    // Write the output to stdout
    public static void translate(String fileName, Map<String,String> dict){

        if (dict == null) {
            System.out.println("Error: no dictionary loaded.");
            return;
        }

        // Check if file exists
        // I think we need the absolute file because we changed user.dir, stupid
        File file=new File(fileName).getAbsoluteFile();

        if (!file.exists()) {
            System.out.println("Error in Tran.translate: file "+file.getPath()+" not found !");
            return;
        }

        System.out.println("Tran.translate: Opening file: "+file.getPath());
        System.out.println("------------------------------------------------------");

        // Open the file
        try (BufferedReader br=new BufferedReader(new FileReader(file))) {

            // Read lines one by one and translate them to stdout
            String line;
            final String UTF8_BOM = "\uFEFF"; // The BOM character
            while ((line=br.readLine())!=null) {
                if (line.startsWith(UTF8_BOM)) {
                    line=line.substring(1); // Check for BOM
                }

                translateLine(line,dict);
            }

        } catch (IOException e) { // If anything goes wrong
            System.out.println("IOException caught in  Tran.translate: ");
            e.printStackTrace();
            return;
        }
        System.out.println("------------------------------------------------------");
    }

    // Translate one line to stdout
    private static void translateLine(String line, Map<String, String> dict) {

        // Stupid Stupid IntelliJ IDEA giving error where there is none
        // unless you turn off regex check.
        // I turned it off, I'll write all my regexs by hand!
        Matcher m=Pattern.compile("\\p{IsAlphabetic}+").matcher(line); // Create the matcher for 1 word

        StringBuilder sb=new StringBuilder(); // Output line

        int prevIndex=0; // The previous index

        while (m.find()) {
            sb.append(line.substring(prevIndex,m.start())); // The non-word chars before the current word
            sb.append(translateWord(m.group(),dict));
            prevIndex=m.end(); // Update the index
        }
        sb.append(line.substring(prevIndex,line.length())); // The last non-word part

        System.out.println(sb.toString()); // Print line to stdout
    }

    // Translate one word with a selected dictionary
    private static String translateWord(String word,Map<String, String> dict) {
        boolean firstUC=Character.isUpperCase(word.charAt(0)); // Is the first char an upper case?

        word=word.toLowerCase();

        String result= dict.get(word); // Get the dictionary word
        if (result == null) result=word.toUpperCase(); // Word not in dictionary
        if (result.equals("_")) result=""; // Empty word


        // Check if char1 is upper case
        if (firstUC && result.length()>0)
            result=Character.toUpperCase(result.charAt(0))+result.substring(1,result.length());

        return result;
    }

    // Load the dictionary from lang1 to lang2
    // Throws exception if dictionary not found
    public static void loadDic(String lang1, String lang2) throws DictionaryLoadException {

        boolean reverse=false; // Should the dictionary be loaded in reverse?


        // Note that dciDir is already absolute
        // So we don't need to make our files absolute
        File file=new File(dicDir,lang1+'_'+lang2+".dic");
        String fileName=file.getPath();


        if (!file.exists()) { // File not found
            System.out.println("Dictionary "+fileName+" not found.");

            // Try the other order of languages
            file=new File(dicDir,lang2+'_'+lang1+".dic");
            fileName=file.getPath();

            System.out.println("Trying "+fileName);

            if (file.exists()) {
                reverse=true; // Load in reverse
            } else {
                throw new DictionaryLoadException("Dictionary "+fileName+" not found.");
            }
        }

        // File found, try to load it

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            System.out.println("Loading dictionary "+fileName);
            dictStr=reverse ? lang1+' '+lang2 : lang2+' '+lang1; // Name for prompt
            dict=new HashMap<String,String>(); // New map for the dictionary

            String line; // Process line after line
            while ((line=br.readLine())!=null) {

                // This is rather peculiar, but I get some funny character
                // coming from the Unicode Byte Order Mark (BOM)
                // It's a known glitch
                // One could have used UnicodeReader from Guava instead
                final String UTF8_BOM = "\uFEFF";

                if (line.startsWith(UTF8_BOM)) {
                    line=line.substring(1);
                }
                loadDicLine(line,reverse);
            }

            if (dict.size()==0) { // No info in the map
                dict=null; // Clean up on failed load
                dictStr=null;
                throw new DictionaryLoadException("Dictionary contains no data");
            } else {
                System.out.println("Dictionary contains "+dict.size()+" entries.");
            }


        }  catch (IOException e) {
            e.printStackTrace();
            dict=null; // Clean up on failed load
            dictStr=null;
            throw new DictionaryLoadException("IOException caught in Tran.loadDic");
        }
    }

    // Parse each line of the dictionary file
    private static void loadDicLine(String line, boolean reverse) {
        Matcher m=Pattern.compile("(\\S+)\\s+(\\S+)").matcher(line); // Create the regex matcher

        if (!m.matches()) {
            System.out.println("Error in dictionary line \""+line+"\", skipping.");
        } else { // Write to the map taking reverse into account
            if (reverse) dict.put(m.group(2),m.group(1));
             else dict.put(m.group(1),m.group(2));
        }

    }


    // As a test, try typing the following commands:
    // help
    // ls
    // tran wrong_name.txt
    // tran sample.txt
    // load en ua
    // tran sample.txt
    // load ua en
    // tran sampleua.txt
    // exit
    public static void main(String[] args) throws IOException {
        consoleUI();
    }


    // A simple console-based interface to translate
    // Note: it would be nicer to use NIO (Path class), but
    // the assignment explicitly asks to use the File class
    // Note: IOException here could only come from System.in
    // Which is not very likely

    private static void consoleUI() throws IOException {

        // Set the current working directory
        // Note how I avoid using system-dependent separators (/ or \)
        File newDir = new File(new File(new File("src","main"),"resources"),"task5");
        System.setProperty("user.dir", newDir.getAbsolutePath() );

        // Absolute path to dictionary directory
        dicDir=new File("dictionaries").getAbsoluteFile();

        // Set up a simple command-line interface
        BufferedReader in=new BufferedReader(new InputStreamReader(System.in));


        System.out.println("Welcome to the simple translator!");
        System.out.println("Type 'help' for more info.");

        dictStr=null; // No dictionary loaded at  the moment
        dict=null;

        // Try to pre-load en_se dictionary to get things going
        loadDic2("load en se");

        for (;;) { // Endless cycle broken by System.exit
            if (dictStr == null) System.out.print(">"); // Print the prompt
             else System.out.print(dictStr+">");


            String line=in.readLine(); // Read line of code

            //Get the predicate for this line
            String predicate=line.split("\\W+",2)[0];


            switch (predicate.toLowerCase()){
                case "help": // Get the help
                    System.out.println("Available commands:");
                    System.out.println("cd <dir>   : Change directory");
                    System.out.println("{exit|quit}  : Quit the program");
                    System.out.println("help  : Print this message");
                    System.out.println("load <lang1> <lang2>  : Load a dictionary");
                    System.out.println("ls  : List files in the current directory");
                    System.out.println("{translate|tran} <file_name>  : Translate a file");
                    break;
                case "cd": // Change directory
                    chdir(line);
                    break;
                case "exit":case "quit":   // Exit
                    System.out.println("See you soon!");
                    System.exit(0);
                    break;
                case "load": // Load a dictionary
                    loadDic2(line);
                    break;
                case "ls": // List files
                    System.out.println("The current directory is : "+System.getProperty("user.dir"));
                    for (String s:new File(".").getAbsoluteFile().list()) System.out.println(s);
                    break;
                case "translate":case "tran": // Translate a file
                    translate2(line);
                    break;
                default:
                    System.out.println("Syntax error !");
                    System.out.println("Type 'help' for more info.");
            }
        }
    }

    // Change directory
    private static void chdir(String line) {
        // Regex cd <dir>
        Matcher m=Pattern.compile("cd\\s+(\\S+)").matcher(line);

        if (!m.matches()) {
            System.out.println("Syntax error! Use:");
            System.out.println("\"cd <dir>\"");
            return;
        }

        String dir=m.group(1);

        // Would be simpler with NIO (Path), but File works almost as well

        try {
            File file=new File(dir).getCanonicalFile(); // This would remove things like ".." and "."
            if (file.exists() && file.isDirectory()){
                System.setProperty("user.dir",file.getPath()); // Set the new dir
            } else {
                System.out.println("Directory "+dir+" not found!");
            }
        } catch (IOException e) { // Can be thrown by getCanonicalFile() if stupid characters in dir
            System.out.println("Directory "+dir+" not found!");
        }
    }

    // A wrapper to loadDic which reads the line like
    // load <lang1> <lang2>
    private static void loadDic2(String line) {
        // Regex for "load <lang1> <lang2>"
        Matcher m=Pattern.compile("load\\s+(\\w+)\\s+(\\w+)").matcher(line);

        if (!m.matches()) {
            System.out.println("Syntax error! Use:");
            System.out.println("\"load <lang1> <lang2>\"");
        } else {
            try {
                loadDic(m.group(1),m.group(2)); // Load the dictionary
            } catch (DictionaryLoadException e) {
                e.printStackTrace();
                System.out.println("Dictionary load failed.");
            }
        }

    }

    // A wrapper to translate which takes a line of the sort
    // "translate filename.txt"
    // "translate c:\mydir\filename.txt"
    // /home/user/mydir/filename.txt
    private static void translate2(String line) {
        // Regex for "translate <file_name>" or "tran <file_name>"
        Matcher m=Pattern.compile("\\w+ (.+)").matcher(line);

        if (!m.find()) { // Cannot use regex
            System.out.println("Syntax error! Use:");
            System.out.println("\"translate <file.name>\" or \"tran <file.name>\"");
        } else {
            String fileName=m.group(1);
            translate(fileName,dict);
        }
    }
}
