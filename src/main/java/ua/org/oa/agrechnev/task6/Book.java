package ua.org.oa.agrechnev.task6;

import java.io.Serializable;

/**
 * Created by Oleksiy Grechnyev on 8/31/2016.
 * A trivial class for book, serializable, realizes equals
 */
public class Book implements Serializable{
    private String author;
    private String title;
    private int year;

    public Book(String author,String title, int year) {
        setAuthor(author);
        setTitle(title);
        setYear(year);
    }

    public Book() { }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        if (this.title == null) this.title="";
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
        if (this.author == null) this.author="";
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (year != book.year) return false;
        if (!title.equals(book.title)) return false;
        return author.equals(book.author);

    }

    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + author.hashCode();
        result = 31 * result + year;
        return result;
    }

    @Override
    public String toString() {
        return "{" +
                "author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", year=" + year +
                '}';
    }
}
