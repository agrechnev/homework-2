package ua.org.oa.agrechnev.task6;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 * Created by Oleksiy Grechnyev on 8/31/2016.
 * Simple file/directory operations used for the book editor
 * Uses nio (I used class File for homework 5)
 */
public class BookFileOps {
    private BookFileOps() {throw new Error(); }

    // The home directory becuase stupid stupid Java does not have a cd operation
    private static Path homeDir;

    // Set the default system directory
    public static void setHomeDir(){
        Path dir= Paths.get("src","main","resources","task6").toAbsolutePath();

        if (!Files.exists(dir) || !Files.isDirectory(dir)) {
            // Not a valid directory
            System.out.println("BookFileOps.setHomeDir: cannot set home directory !!!");
            System.exit(1);
        }
        homeDir=dir;
    }

    // Get the absolute file name for current home directory
    public static String getAbsName(String fileName){
        return homeDir.resolve(fileName).toString();
    }

    public static Path getHomeDir() {
        return homeDir;
    }

    // Change directory
    public static void fileCD(String dirName){
        Path newDir;
        try {
            newDir = homeDir.resolve(dirName).toRealPath();
        } catch (IOException e) {
            System.out.println("Error: Cannot change into directory "+dirName);
            return;
        }

        if (Files.isReadable(newDir) && Files.isDirectory(newDir)) {
            homeDir=newDir;
        } else {
            System.out.println("Error: Cannot change into directory "+dirName);
        }
    }

    // Display current directory
    public static void fileLS(){
        System.out.println("Current directory : "+homeDir);

        try {
            Files.list(homeDir).forEach(p->System.out.println(p.getFileName()));
        } catch (IOException e) {
            // Just ignore, but it shouldn't happen
            e.printStackTrace();
        }
    }

    //Delete a file
    public static void fileRM(String fileName){
        Path file=homeDir.resolve(fileName);

        try {
            Files.delete(file);
        } catch (IOException e) {
            System.out.println("Error: cannot delete file "+file);
        }
    }

    //Move/rename file
    public static void fileMV(String sourceName,String destName){
        Path source=homeDir.resolve(sourceName);
        Path dest=homeDir.resolve(destName);

        // If dest is a directory, add source file name to it
        // To make it work like unix mv
        if (Files.exists(dest) && Files.isDirectory(dest)) {
            dest=dest.resolve(source.getFileName());
        }

        try {
            Files.move(source,dest,REPLACE_EXISTING);
        } catch (IOException e) {
            System.out.println("Error: cannot move file "+source);
            System.out.println("to destination "+dest);
        }
    }

    // Move/rename file wrapper
    // line should contain 2 file names
    public static void fileMV2(String line){
        String[] split=line.split("\\s+",2);

        if (split.length<2) {
            System.out.println("Syntax error! Use:");
            System.out.println("file mv <source> <dest>");
        } else fileMV(split[0],split[1]);
    }


    //Copy file
    public static void fileCP(String sourceName,String destName){
        Path source=homeDir.resolve(sourceName);
        Path dest=homeDir.resolve(destName);

        // If dest is a directory, add source file name to it
        // To make it work like unix cp
        if (Files.exists(dest) && Files.isDirectory(dest)) {
            dest=dest.resolve(source.getFileName());
        }

        try {
            Files.copy(source,dest,REPLACE_EXISTING);
        } catch (IOException e) {
            System.out.println("Error: cannot copy file "+source);
            System.out.println("to destination "+dest);
        }
    }

    // Copy file wrapper
    // line should contain 2 file names
    public static void fileCP2(String line){
        String[] split=line.split("\\s+",2);

        if (split.length<2) {
            System.out.println("Syntax error! Use:");
            System.out.println("file cp <source> <dest>");
        } else fileCP(split[0],split[1]);
    }

    // Create a directory
    public static void fileMKDIR(String line){
        Path dir=homeDir.resolve(line);

        try {
            Files.createDirectory(dir);
        } catch (IOException e) {
            System.out.println("Error: cannot create directory "+dir);
        }
    }

}
