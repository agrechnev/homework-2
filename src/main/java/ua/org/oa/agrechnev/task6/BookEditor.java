package ua.org.oa.agrechnev.task6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ua.org.oa.agrechnev.task6.BookFileOps.*;
import static ua.org.oa.agrechnev.task6.BookIO.*;

/**
 * Created by Oleksiy Grechnyev on 8/31/2016.
 * A simple text-mode book editor
 */
public class BookEditor {
    private BookEditor() {throw new Error(); }

    private static List<Book> library; // The library

    public static void main(String[] args) throws IOException {
        runUI();
    }

    // Run a simple text-mode UI
    // IOException can only come from System.in, unlikely
    private static void runUI() throws IOException {
        setHomeDir();
        library=new ArrayList<Book>(); // Empty library
        BufferedReader in=new BufferedReader(new InputStreamReader(System.in)); // The proper console in

        System.out.println("Welcome to the book editor.");
        System.out.println("Type 'help' for more info.");
        System.out.println("Type 'import lib1.txt' for a quick start.");

        for(;;){ //The infinite cycle of the UI
            System.out.print(">"); // Prompt
            String line=in.readLine(); // Read line of code

            //Get the command word for this line
            line=line.trim();
            String[] split = line.split("\\s+", 2); // Split into 2 parts
            String command= split[0].toLowerCase();
            line=(split.length>1) ? split[1].trim(): ""; // The remaining part of the line

            String command2=""; // Second command word if command="file"
            if (command.equals("file") && line.length()>0) {
                // Repeat the procedure to get the second command word
                split = line.split("\\s+", 2); // Split into 2 parts
                command2= split[0].toLowerCase();
                line=(split.length>1) ? split[1].trim(): ""; // The remaining part of the line
            }

            switch (command){

                case "add": // Add a book
                    runAdd(line);
                    break;

                case "clear": // Clear the library
                    library=new ArrayList<Book>(); // Empty library
                    break;

                case "delete": // Delete a book by number
                    runDelete(line);
                    break;

                case "exit": //Exit the program
                    System.out.println("See you soon!");
                    System.exit(0);
                    break;

                case "export": //Import data from a text file
                    runExport(line,false);
                    break;

                case "file": // File operations
                    switch(command2){
                        case("cd"): // Change dir
                            fileCD(line);
                            break;

                        case("cp"): // Copy file
                            fileCP2(line);
                            break;

                        case("ls"): // Display dir
                            fileLS();
                            break;

                        case("mkdir"): // Create directory
                            fileMKDIR(line);
                            break;

                        case("rm"): // Delete file
                            fileRM(line);
                            break;

                        case("mv"): // Move/rename file
                            fileMV2(line);
                            break;

                        default:
                            System.out.println("Syntax error!");
                            System.out.println("Type 'help' for more info.");
                    }
                    break;

                case "help": // The help message
                    System.out.println("Library operations:");
                    System.out.println("add Author;Title;Year       : Add a book");
                    System.out.println("clear                       : Empty the library");
                    System.out.println("delete <book_number>        : Delete a book");
                    System.out.println("exit                        : Exit program");
                    System.out.println("export <file>               : Export library to a text file");
                    System.out.println("help                        : Print this message");
                    System.out.println("import <file>               : Import library from a text file");
                    System.out.println("importa <file>              : Append library from a text file");
                    System.out.println("list                        : List all books in the library");
                    System.out.println("load                        : Load library from a bin file (deserialize)");
                    System.out.println("loada                       : Append library from a bin file (deserialize)");
                    System.out.println("save                        : Save library to a bin file (serialize)");
                    System.out.println("sort {a,t,y} {a,d}          : Sort the library");
                    System.out.println("");
                    System.out.println("File operations:");
                    System.out.println("file cd                     : Change current directory");
                    System.out.println("file cp <source> <dest>     : Copy file");
                    System.out.println("file ls                     : Display current directory");
                    System.out.println("file mkdir <dir>            : Create a new directory");
                    System.out.println("file rm <file>              : Delete file or empty directory");
                    System.out.println("file mv <source> <dest>     : Move/rename file");
                    break;

                case "import": //Import data from a text file
                    runImport(line,false);
                    break;

                case "importa": //Import data from a text file and append to library
                    runImport(line,true);
                    break;

                case "list": // List all books
                    for (int i = 0; i < library.size(); i++) {
                        System.out.println(i+":"+library.get(i));
                    }
                    break;


                case "load": // Load the library (deserialize)
                    runLoad(line,false);
                    break;

                case "loada": // Append the library (deserialize)
                    runLoad(line,true);
                    break;

                case "save": // Save the library (serialize)
                    runSave(line);
                    break;

                case "sort": // Sort the library
                    runSort(line);
                    break;
                default:
                    System.out.println("Syntax error!");
                    System.out.println("Type 'help' for more info.");
            }
        }
    }

    // Sort the library
    private static void runSort(String line) {
        String str=line.toLowerCase();
        Matcher m= Pattern.compile("([aty])\\s+([ad])").matcher(str);

        if (!m.matches()) {
            System.out.println("Syntax error! Use:");
            System.out.println("sort {a,t,y}  {a,d}");
            return;
        }

        // Set up the comparator
        Comparator<Book> comp=null;
        switch (m.group(1)) {
            case "a": // Sort by author
                if (m.group(2).equals("a")) { // Ascending orders
                    comp=(b1,b2) -> b1.getAuthor().compareTo(b2.getAuthor());
                } else { // Descending order
                    comp=(b1,b2) -> b2.getAuthor().compareTo(b1.getAuthor());
                }
                break;

            case "t": // Sort by title
                if (m.group(2).equals("a")) { // Ascending order
                    comp=(b1,b2) -> b1.getTitle().compareTo(b2.getTitle());
                } else { // Descending order
                    comp=(b1,b2) -> b2.getTitle().compareTo(b1.getTitle());
                }
                break;

            case "y": // Sort by year
                if (m.group(2).equals("a")) { // Ascending order
                    comp=(b1,b2) -> Integer.compare(b1.getYear(),b2.getYear());
                } else { // Descending order
                    comp=(b1,b2) -> Integer.compare(b2.getYear(),b1.getYear());
                }
                break;
        }

        // Sort the library
        Collections.sort(library,comp);
    }

    // Delete a book
    private static void runDelete(String line) {
        try {
            int bn=Integer.parseInt(line);
            if (bn<0 || bn>=library.size()) {
                System.out.println("Error: Book number out of range.");
            } else library.remove(bn);

        } catch(NumberFormatException e) {
            System.out.println("Syntax error! Use:");
            System.out.println("delete <book_number>");
        }
    }

    // Export library to a text file
    private static void runExport(String line, boolean append) {

        if (line.length()==0) {
            System.out.println("Syntax error! Use:");
            System.out.println("export <file.name>");
            return;
        }

        String fileName=getAbsName(line);

        try {
            writeBooks(fileName,library,append); // Write the books
            System.out.println("Library exported successfully.");
        } catch (IOException e) {
            System.out.println("Error: Cannot write file "+fileName);
        }
    }



    // Save library to a bin file (serialize)
    private static void runSave(String line) {

        if (line.length()==0) {
            System.out.println("Syntax error! Use:");
            System.out.println("save <file.name>");
            return;
        }

        String fileName=getAbsName(line);

        try {
            serializeBooks(fileName,library); // Write the books
            System.out.println("Library saved successfully.");
        } catch (IOException e) {
            System.out.println("Error: Cannot write file "+fileName);
        }
    }


    // Add a book to library
    private static void runAdd(String line) {

        Book book=parseBook(line);
        if (book == null) {
            System.out.println("Syntax error! Use:");
            System.out.println("add Author;Title;Year");
            return;
        }

        if (library.contains(book)) {
            System.out.println("Error: Book already in library.");
        } else {
            library.add(book);
            System.out.println("Book added successfully.");
        }
    }

    // Import data from a text file
    // If append==true append to existing library
    private static void runImport(String line, boolean append) {
        if (line.length()==0) {
            System.out.println("Syntax error! Use:");
            System.out.println("import <file.name>");
            return;
        }

        String fileName=getAbsName(line);
        try {

            // Try to read the books
            Collection<Book> newLibrary=readBooks(fileName);
            if (newLibrary.size()==0) {
                System.out.println("Error: No data in file "+fileName);
            } else {
                System.out.println("Imported " + newLibrary.size() + " books.");
                if (append) {
                    library.addAll(newLibrary); // Add imported books
                    // Eliminate duplicates
                    library = new ArrayList<Book>(new LinkedHashSet<Book>(library));

                } else {
                    // Convert to ArrayList via LinkedHashSet
                    // To eliminate duplicates
                    library = new ArrayList<Book>(new LinkedHashSet<Book>(newLibrary));
                }
            }

        } catch (IOException e) {
            System.out.println("Error: Cannot read file "+fileName);
        }
    }

    // Load data from a bin file (deserialize)
    // If append==true append to existing library
    private static void runLoad(String line, boolean append) {
        if (line.length()==0) {
            System.out.println("Syntax error! Use:");
            System.out.println("load <file.name>");
            return;
        }

        String fileName=getAbsName(line);
        try {

            // Try to read the books
            Collection<Book> newLibrary=deserializeBooks(fileName);
            if (newLibrary.size()==0) {
                System.out.println("Error: No data in file "+fileName);
            } else {
                System.out.println("Imported " + newLibrary.size() + " books.");
                if (append) {
                    library.addAll(newLibrary); // Add imported books
                    // Eliminate duplicates
                    library = new ArrayList<Book>(new LinkedHashSet<Book>(library));

                } else {
                    // Convert to ArrayList via LinkedHashSet
                    // To eliminate duplicates
                    library = new ArrayList<Book>(new LinkedHashSet<Book>(newLibrary));
                }
            }

        } catch (IOException e) {
            System.out.println("Error: Cannot read file "+fileName);
        } catch (ClassNotFoundException e) {
            System.out.println("Error: No valid data in file "+fileName);
        }
    }

}
