package ua.org.oa.agrechnev.task6;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Oleksiy Grechnyev on 8/31/2016.
 * Utility class for IO operations with books
 * Both binary (serialization) and text format
 * Uses absolute file paths to take user.dir into acoount
 */
public class BookIO {
    private BookIO() {throw new Error(); } // No instantiation ever!

    // Serialize a book collection (write as binary file)
    // IOException if anything goes wrong
    public static void serializeBooks(String fileName, Collection<Book> books) throws IOException{
        try (
           ObjectOutputStream out=new ObjectOutputStream(
                   new BufferedOutputStream(
                           new FileOutputStream(fileName)))
        ){
            out.writeObject(books);
        }
    }

    // De-serialize a book collection (write as binary file)
    // IOException or ClassNotFoundException if anything goes wrong
    public static Collection<Book> deserializeBooks(String fileName) throws IOException, ClassNotFoundException {
        try (
                ObjectInputStream in=new ObjectInputStream(new BufferedInputStream(
                        new FileInputStream(fileName)))
        ){
            return (Collection<Book>)in.readObject();
        }
    }

    // Write a book collection as a text file
    // append=true appends to the existing file, rather than overwrite
    // Separated with the ';' characters
    // Esc sequences: use \\ for \ and \; for ;
    // IOException if anything goes wrong
    public static void writeBooks(String fileName, Collection<Book> books, boolean append) throws IOException{
        try (
                PrintWriter out=new PrintWriter(new BufferedWriter(
                        new FileWriter(fileName,append)));
        ){
            books.forEach(b->writeBook(b,out));
        }
    }

    // Read a book collection from a text file
    // Separated with the ';' characters
    // Esc sequences: use \\ for \ and \; for ;
    // IOException if anything goes wrong
    public static ArrayList<Book> readBooks(String fileName) throws IOException {
        try (BufferedReader in=new BufferedReader(new FileReader(fileName))){
            ArrayList<Book> books=new ArrayList<>();

            Book b;
            while ((b=readBook(in))!=null) books.add(b);
            return books;
        }
    }

    // Read a single book from a line of text file
    // Try to the next line if the record is corrupt
    // Or return null if EOF
    private static Book readBook(BufferedReader in) throws IOException {
        for (;;) {
            String line=in.readLine(); // Read the line
            if (line == null) return null; // End of file

            Book book=parseBook(line);
            if (book!=null) return book;
            // If no match read the next line
        }
    }

    // Parse a book from a single book line
    // Returns null if unsuccessful
    public static Book parseBook(String line){
        // Try to match the line with pattern String;String;number
        Matcher m= Pattern.compile("(.*[^\\\\]);(.*[^\\\\]);\\s*(\\d+)\\s*").matcher(line);
        if (m.matches()) { // Return this book
            return new Book(ESCUnWrap(m.group(1)),ESCUnWrap(m.group(2)),Integer.parseInt(m.group(3)));
        } else return null;
    }

    // Write a single book as a line in file out
    private static void writeBook(Book b, PrintWriter out) {
        StringBuilder line=new StringBuilder(ESCWrap(b.getAuthor())); // Book title

        out.println(ESCWrap(b.getAuthor())+";"+ESCWrap(b.getTitle())+";"+b.getYear());
    }

    // Trim
    // Remove any trailing backslashes (too much trouble otherwise)
    // Change ";" to "\;"
    private static String ESCWrap(String s) {
        String str=s;

        str.trim();
        while (str.endsWith("\\")) str=str.substring(0,str.length()-1);
        return str.replace(";","\\;");
    }

    // Trim
    // Remove any \'s at the end of line
    // Change "\;" to ";"
    private static String ESCUnWrap(String s)
    {
        String str=s.trim();
        // Remove trailing backslashes which could have appeared after trim
        while (str.endsWith("\\")) str=str.substring(0,str.length()-1);

        return str.replace("\\;",";");
    }

}
