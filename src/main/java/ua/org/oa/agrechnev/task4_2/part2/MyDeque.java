package ua.org.oa.agrechnev.task4_2.part2;

/**
 * Created by Oleksiy Grechnyev on 8/22/2016.
 * A double-ended queue interface
 * Similar to Deque, but simpler
 * This version extends Iterable to iterate over the deque
 */
public interface MyDeque<E> extends Iterable<E>{



    // Add element at the front
    void addFirst(E e);

    // Add element at the end
    void addLast(E e);

    // Get the first element and remove it
    E removeFirst();

    // Get the last element and remove it
    E removeLast();

    // Get the first element, does not delete
    E getFirst();

    // Get the last element, does not delete
    E getLast();

    // Check if the list contains an element
    boolean contains(E e);

    // Clean up (remove ALL elements)
    void clear();

    // Return the list as array
    E[] toArray();

    // Get the size of the list
    int size();

    // Check if the list contains ALL elements of the MyDeque object de
    boolean containsAll(MyDeque<? extends E> de);

}
