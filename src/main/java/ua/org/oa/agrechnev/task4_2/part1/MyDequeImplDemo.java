package ua.org.oa.agrechnev.task4_2.part1;

/**
 * Created by Oleksiy Grechnyev on 8/18/2016.
 * A simple demo for MyDequeImpl
 */
public class MyDequeImplDemo {
    public static void main(String[] args) {
        String[] strings={"Alpha","Beta","Gamma","Delta"};

        System.out.println("A simple demo for MyDequeImpl :");

        MyDeque<String> deque=new MyDequeImpl<>();

        System.out.println("--------------------");
        System.out.println("Using deque as queue (first to last):");
        for (int i = 0; i < 4; i++)  deque.addFirst(strings[i]);
        System.out.println("size="+deque.size());
        while (deque.size()>0) System.out.println(deque.removeLast());

        System.out.println("--------------------");
        System.out.println("Using deque as queue (last to first):");
        for (int i = 0; i < 4; i++)  deque.addLast(strings[i]);
        System.out.println("size="+deque.size());
        while (deque.size()>0) System.out.println(deque.removeFirst());

        System.out.println("--------------------");
        System.out.println("Using deque as stack (first to first):");
        for (int i = 0; i < 4; i++)  deque.addFirst(strings[i]);
        System.out.println("size="+deque.size());
        while (deque.size()>0) System.out.println(deque.removeFirst());

        System.out.println("--------------------");
        System.out.println("Using deque as stack (last to last):");
        for (int i = 0; i < 4; i++)  deque.addLast(strings[i]);
        System.out.println("size="+deque.size());
        while (deque.size()>0) System.out.println(deque.removeLast());

        System.out.println("--------------------");
        System.out.println("Constructor and containsAll:");
        for (int i = 0; i < 4; i++)  deque.addLast(strings[i]);
        MyDeque<String> d2=new MyDequeImpl<>(deque); // Copy the queue
        System.out.println(deque.containsAll(d2));
        deque.removeLast();
        System.out.println(deque.containsAll(d2));
    }
}
