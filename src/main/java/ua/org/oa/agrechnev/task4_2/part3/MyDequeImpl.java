package ua.org.oa.agrechnev.task4_2.part3;

import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * Created by Oleksiy Grechnyev on 8/23/2016.
 */
public class MyDequeImpl<E> implements MyDeque<E> {

    //------------------------
    // The iterator class
    // Here it uses fields of the MyDequeImpl object
    // And works with the linked list directly
    // It is important that it is a (non-static) inner class

    private class ListIteratorImpl<T> implements ListIterator<T> {

        Node<T> next_pos; // Next position (for hasNext, next)
        Node<T> prev_pos; // Previous position (for hasPrevious, previous)
        Node<T> last_pos; // Last position (for remove)

        int cursor; // The next index after cursor position
        boolean remove_illegal; // Is it illegal to remove  or set now?

        // Constructor
        public ListIteratorImpl() {

            remove_illegal =true; // Cannot remove before first next

            // Stupid type casts, but I guess Java does not know
            // That T of Iterator is the same as E of MyDequeImpl
            next_pos=(Node<T>)head;
            prev_pos=null;
            last_pos=null;

            cursor=0;

        }

        @Override
        // Is there a next element?
        public boolean hasNext() {
            return next_pos!=null;
        }

        @Override
        // Is there a previous element?
        public boolean hasPrevious() { return prev_pos!=null; }

        @Override
        public int nextIndex() { return cursor; }

        @Override
        public int previousIndex() { return cursor-1; }

        @Override
        // Get the next element
        public T next() {

            if (next_pos == null) { // No next element
                throw new NoSuchElementException(
                        "No next element in MyDequeImpl<E>.ListIteratorImpl<E>.next()");
            }

            remove_illegal =false; // Clear the remove flag: remove is allowed now
            last_pos=next_pos; // Save the old next_pos for remove
            prev_pos=next_pos;
            next_pos=next_pos.next;

            cursor++; // Move the cursor 1 position right
            return last_pos.data; // Return the data of the previous position
        }

        @Override
        // Get the previous element
        public T previous() {
            if (prev_pos == null) { // No previous element
                throw new NoSuchElementException(
                        "No previous element in MyDequeImpl<E>.ListIteratorImpl<E>.previous()");
            }

            remove_illegal =false; // Clear the remove flag: remove is allowed now
            last_pos=prev_pos; // Save the old prev_pos for remove
            next_pos=prev_pos;
            prev_pos=prev_pos.prev;

            cursor--; // Move the cursor 1 position left
            return last_pos.data; // Return the data of the previous position
        }

        @Override
        // Remove the element last_pos (last read by next or previous)
        public void remove() {
            if (remove_illegal || last_pos==null) {
                throw new IllegalStateException("Wrong use of MyDequeImpl<E>.ListIteratorImpl<E>.remove()");
            }
            // Set the remove flag: illegal to remove now until next() or previous() is called
            remove_illegal =true;

            // Update next_pos, prev_pos and cursor
            if (prev_pos==last_pos) { // Removing the element LEFT of cursor after next()
                cursor--;
            }
            prev_pos=last_pos.prev; // Now point to the left & right of the removed element
            next_pos=last_pos.next;

            // Unlink this element from the list
            // Remember: head, tail and size are the fields or the MyDequeImpl object!
            // Unlink left
            if (prev_pos==null) { // First element in the list
                head= (Node<E>) next_pos; // Stupid casts again
            } else {
                prev_pos.next=next_pos;
            }

            // Unlink right
            if (next_pos==null) { // Last element in the list
                tail= (Node<E>) prev_pos;
            } else {
                next_pos.prev=prev_pos;
            }

            size--; // Don't forget the size

            last_pos.prev=null; // Help the GC
            last_pos.next=null;
            last_pos.data=null;
            last_pos=null;

        }

        @Override
        // Change the element at last_pos
        public void set(T t) {
            if (remove_illegal || last_pos==null) {
                throw new IllegalStateException("Wrong use of MyDequeImpl<E>.ListIteratorImpl<E>.set()");
            }
            // Does not set remove_illegal since set is perfectly harmless

            last_pos.data=t; // Set the data. Simple.
        }

        @Override
        // Add the element at the cursor position, and move the cursor to the right of it
        public void add(T t) {
            remove_illegal=true; // Always set the flag, but no need to check it

            Node<T> newNode=new Node<T>(t,prev_pos,next_pos);

            // Link the new node to the left
            if (prev_pos==null) { // New node is first in the list
                head=(Node<E>)newNode;
            } else {
                prev_pos.next=newNode;
            }

            // Link the new node to the right
            if (next_pos==null) { // New node is last in the list
                tail=(Node<E>)newNode;
            } else {
                next_pos.prev=newNode;
            }

            prev_pos=newNode; // Cursor is RIGHT of the new node by definition

            // Don't forget the cursor and size
            cursor++;
            size++;
        }
    }


    //------------------------
    // A class for nodes: data and 2 links
    private static class Node<E>{
        E data; // Data
        Node<E> next; // Next node
        Node<E> prev; // Previous node

        private Node(E data, Node<E> prev, Node<E> next) {
            this.data = data;
            this.next = next;
            this.prev = prev;
        }
    }

    // END of nested classes
    //------------------------
    // Fields of MyDequeImpl

    private Node<E> head; // First element of the list
    private Node<E> tail; // Last element of the list
    private int size; // Size of the list

    // Constructor: sets up an empty list
    public MyDequeImpl() {
        head=null;
        tail=null;
        size=0;
    }
    // Constructor: clones another deque object
    public MyDequeImpl(MyDeque<E> deque) {
        this(); // Basic constructor

        for (E e:deque.toArray()) addLast(e); // Copy the data
    }



    // Add element at the front
    @Override
    public void addFirst(E e) {
        final Node<E> newNode=new Node<E>(e,null,head); // Create a new node

        if (head == null) { // Empty list: link new node as both head and tail
            head=newNode;
            tail=newNode;
        } else { // Link head and the former first element
            head.prev=newNode;
            head=newNode;
        }
        size++;  // Don't forget to increase the size
    }

    // Add element at the back
    @Override
    public void addLast(E e) {
        final Node<E> newNode=new Node<E>(e,tail,null); // Create a new node

        if (tail == null) { // Empty list: link new node as both head and tail
            head=newNode;
            tail=newNode;
        } else { // Link tail and the former last element
            tail.next=newNode;
            tail=newNode;
        }
        size++;  // Don't forget to increase the size
    }

    // Remove and returns the first element
    @Override
    public E removeFirst() {
        // Exception if the list is empty
        if (head == null) throw new NoSuchElementException();

        final E data=head.data; // Save the value

        head.data=null; // For better garbage collection

        if (head.next==null) { // Only one element in the list
            head=null;
            tail=null;
        } else { // Unlink the first element from the rest
            final Node<E> newHead=head.next;
            newHead.prev=null;
            head.next=null; // For better garbage collection
            head=newHead; // New fist element
        }


        size--; // Decrease size
        return data;

    }

    // Remove and returns the last element
    @Override
    public E removeLast() {
        // Exception if the list is empty
        if (tail == null) throw new NoSuchElementException();

        final E data=tail.data; // Save the value

        tail.data=null; // For better garbage collection

        if (tail.prev==null) { // Only one element in the list
            head=null;
            tail=null;
        } else { // Unlink the last element from the rest
            final Node<E> newTail=tail.prev;
            newTail.next=null;
            tail.prev=null; // For better garbage collection
            tail=newTail; // New fist element
        }


        size--; // Decrease size
        return data;
    }

    // Get the first element
    @Override
    public E getFirst() {
        // Exception if the list is empty
        if (head == null) throw new NoSuchElementException();

        return head.data;
    }

    // Get the last element
    @Override
    public E getLast() {
        // Exception if the list is empty
        if (tail == null) throw new NoSuchElementException();

        return tail.data;
    }

    // Check if the list contains an element
    @Override
    public boolean contains(E e) {

        Node<E> node=head;

        while (node!=null) {
            if (node.data.equals(e)) return true;
            node=node.next; // Go down the list
        }

        return false; // Not found
    }

    // Clean up (remove ALL elements)
    @Override
    public void clear() {
        // Probably it would be enough to nullify head and tail,
        // But my algorithm ensures efficient garbage collection
        while (head!=null) removeFirst();

        size=0;
    }

    @Override
    public E[] toArray() {

        if (head==null) return (E[])new Object[]{}; // Empty array

        Object[] oArray=new Object[size];

        Node<E> node=head;
        for (int i = 0; i < size; i++) {
            if (node==null) throw new Error("Internal error in MyDequeImpl !");

            oArray[i]=node.data;

            node=node.next; // Go to the next node
        }

        return (E[])oArray;
    }

    // Get the size of the list
    @Override
    public int size() {
        return size;
    }

    // Check if the list contains ALL elements of the MyDeque object de
    @Override
    public boolean containsAll(MyDeque<? extends E> de) {
        // Wee need de.toArray(), otherwise cannot access
        // all elements of de without destroying
        // Check every one in turn with this.contains

        for (E e : de.toArray()) if (!contains(e)) return false;

        return true;
    }

    // Return the iterator: 2 versions
    @Override
    public Iterator<E> iterator() {
        return new ListIteratorImpl<E>();
    }

    @Override
    public ListIterator<E> listIterator() {
        return new ListIteratorImpl<E>();
    }
}
