package ua.org.oa.agrechnev.task4_2.part3;

import java.util.ListIterator;

/**
 * Created by Oleksiy Grechnyev on 8/23/2016.
 *
 * Extension of interface Iterable<T>
 * Now returns a ListIterator
 */
public interface ListIterable<T> extends Iterable<T>{
    // Create a ListIterator for this object
    public ListIterator<T> listIterator();
}
