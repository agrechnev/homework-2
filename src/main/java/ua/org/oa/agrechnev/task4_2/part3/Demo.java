package ua.org.oa.agrechnev.task4_2.part3;

import java.util.ListIterator;

/**
 * Created by Oleksiy Grechnyev on 8/23/2016.
 */
public class Demo {
    public static void main(String[] args) {
        // Create a sample list using the ListIterator only
        MyDeque<String> deque=new MyDequeImpl<>();
        ListIterator<String> listIterator=deque.listIterator();

        listIterator.add("Orc");
        listIterator.add("Goblin");
        listIterator.add("Dwarf");
        listIterator.add("Halfling");
        listIterator.previous(); // Go 2 steps left (before Dwarf)
        listIterator.previous();
        listIterator.add("Human");
        listIterator.add("Elf");

        System.out.println("--------------");
        System.out.println("Demo foreach:");
        // Print with foreach loop, uses another temp iterator
        for (String s : deque) System.out.println(s);

        // Now use our old iterator
        System.out.println("--------------");
        System.out.println("Demo while (move right):");
        System.out.println("cursor="+listIterator.nextIndex());
        // Move all the way left
        while (listIterator.hasPrevious()) listIterator.previous();
        System.out.println("cursor="+listIterator.nextIndex());
        // Move right and print
        while (listIterator.hasNext()) System.out.println(listIterator.next());
        System.out.println("cursor="+listIterator.nextIndex());

        System.out.println("--------------");
        System.out.println("Demo while (move left):");
        while (listIterator.hasPrevious()) System.out.println(listIterator.previous());
        System.out.println("cursor="+listIterator.nextIndex());

        // Move right and change Human to Tiefling
        System.out.println("--------------");
        System.out.println("Changing human to tiefling:");
        while (listIterator.hasNext()) {
            if (listIterator.next().equals("Human")) listIterator.set("Tiefling");
        }
        System.out.println("cursor="+listIterator.nextIndex());

        System.out.println("Move left and delete everything:");
        while (listIterator.hasPrevious()) {
            System.out.println(listIterator.previous());
            listIterator.remove();
        }
        System.out.println("cursor="+listIterator.nextIndex());
        System.out.println("size="+deque.size());

        // Create it again
        System.out.println("--------------");
        System.out.println("Creating the list again:");

        listIterator.add("Orc");
        listIterator.add("Goblin");
        listIterator.add("Dwarf");
        listIterator.add("Halfling");
        listIterator.previous(); // Go 2 steps left (before Dwarf)
        listIterator.previous();
        listIterator.add("Human");
        listIterator.add("Elf");

        // Move all the way left
        while (listIterator.hasPrevious()) listIterator.previous();

        System.out.println("Now let us remove Human and Dwarf,");
        System.out.println("And add dragon in the middle");
        // Now move right and remove
        while (listIterator.hasNext())
            switch (listIterator.next()) {
                case "Human":case "Dwarf":
                    listIterator.remove();
            }

        listIterator.previous();
        listIterator.previous();
        listIterator.add("Dragon");

        System.out.println("The result is:");
        for (String s : deque) System.out.println(s);
    }
}
