package ua.org.oa.agrechnev.task4_2.part2;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by Oleksiy Grechnyev on 8/22/2016.
 */
public class MyDequeImpl<E> implements MyDeque<E> {

    //------------------------
    // The iterator class
    // Here it uses fields of the MyDequeImpl object
    // And works with the linked list directly
    // It is important that it is a (non-static) inner class

    private class IteratorImpl<T> implements Iterator<T> {

        Node<T> next_pos; // Next position (for hasNext, next)
        Node<T> last_pos; // Last position (for remove)
        boolean remove_illegal; // Is it illegal to remove now?

        // Constructor
        public IteratorImpl() {

            remove_illegal =true; // Cannot remove before first next

            // Stupid type casts, but I guess Java does not know
            // That T of Iterator is the same as E of MyDequeImpl
            next_pos=(Node<T>)head;
            last_pos=null;

        }

        @Override
        // Is there a next element?
        public boolean hasNext() {
            return next_pos!=null;
        }

        @Override
        // Get the next element
        public T next() {

            if (next_pos == null) { // No next element
                throw new NoSuchElementException("No next element in MyDequeImpl<E>.IteratorImpl<E>.next()");
            }

            remove_illegal =false; // Clear the remove flag: remove is allowed now
            last_pos=next_pos; // Save the old next_pos for remove
            next_pos=next_pos.next;


            return last_pos.data; // Return the data of the previous position
        }

        @Override
        // Remove the element next_pos.prev (last read by next)
        public void remove() {
            if (remove_illegal || last_pos==null) {
                throw new IllegalStateException("Wrong use of MyDequeImpl<E>.IteratorImpl<E>.remove()");
            }
            remove_illegal =true; // Set the remove flag: illegal to remove now until next() is called


            //Unlink this element from the list
            if (last_pos.prev==null) { // First element in the list
                head= (Node<E>) last_pos.next; // Stupid casts again
            } else {
                last_pos.prev.next=last_pos.next;
            }

            if (last_pos.next==null) { // Last element in the list
                tail= (Node<E>) last_pos.prev;
            } else {
                last_pos.next.prev=last_pos.prev;
            }

            size--; // Don't forget the size

            last_pos.prev=null; // Help the GC
            last_pos.next=null;
            last_pos.data=null;
            last_pos=null;

        }
    }


    //------------------------
    // A class for nodes: data and 2 links
    private static class Node<E>{
        E data; // Data
        Node<E> next; // Next node
        Node<E> prev; // Previous node

        private Node(E data, Node<E> prev, Node<E> next) {
            this.data = data;
            this.next = next;
            this.prev = prev;
        }
    }
    //------------------------
    // Fields of MyDequeImpl

    private Node<E> head; // First element of the list
    private Node<E> tail; // Last element of the list
    private int size; // Size of the list

    // Constructor: sets up an empty list
    public MyDequeImpl() {
        head=null;
        tail=null;
        size=0;
    }
    // Constructor: clones another deque object
    public MyDequeImpl(MyDeque<E> deque) {
        this(); // Basic constructor

        for (E e:deque.toArray()) addLast(e); // Copy the data
    }



    // Add element at the front
    @Override
    public void addFirst(E e) {
        final Node<E> newNode=new Node<E>(e,null,head); // Create a new node

        if (head == null) { // Empty list: link new node as both head and tail
            head=newNode;
            tail=newNode;
        } else { // Link head and the former first element
            head.prev=newNode;
            head=newNode;
        }
        size++;  // Don't forget to increase the size
    }

    // Add element at the back
    @Override
    public void addLast(E e) {
        final Node<E> newNode=new Node<E>(e,tail,null); // Create a new node

        if (tail == null) { // Empty list: link new node as both head and tail
            head=newNode;
            tail=newNode;
        } else { // Link tail and the former last element
            tail.next=newNode;
            tail=newNode;
        }
        size++;  // Don't forget to increase the size
    }

    // Remove and returns the first element
    @Override
    public E removeFirst() {
        // Exception if the list is empty
        if (head == null) throw new NoSuchElementException();

        final E data=head.data; // Save the value

        head.data=null; // For better garbage collection

        if (head.next==null) { // Only one element in the list
            head=null;
            tail=null;
        } else { // Unlink the first element from the rest
            final Node<E> newHead=head.next;
            newHead.prev=null;
            head.next=null; // For better garbage collection
            head=newHead; // New fist element
        }


        size--; // Decrease size
        return data;

    }

    // Remove and returns the last element
    @Override
    public E removeLast() {
        // Exception if the list is empty
        if (tail == null) throw new NoSuchElementException();

        final E data=tail.data; // Save the value

        tail.data=null; // For better garbage collection

        if (tail.prev==null) { // Only one element in the list
            head=null;
            tail=null;
        } else { // Unlink the last element from the rest
            final Node<E> newTail=tail.prev;
            newTail.next=null;
            tail.prev=null; // For better garbage collection
            tail=newTail; // New fist element
        }


        size--; // Decrease size
        return data;
    }

    // Get the first element
    @Override
    public E getFirst() {
        // Exception if the list is empty
        if (head == null) throw new NoSuchElementException();

        return head.data;
    }

    // Get the lasst element
    @Override
    public E getLast() {
        // Exception if the list is empty
        if (tail == null) throw new NoSuchElementException();

        return tail.data;
    }

    // Check if the list contains an element
    @Override
    public boolean contains(E e) {

        Node<E> node=head;

        while (node!=null) {
            if (node.data.equals(e)) return true;
            node=node.next; // Go down the list
        }

        return false; // Not found
    }

    // Clean up (remove ALL elements)
    @Override
    public void clear() {
        // Probably it would be enough to nullify head and tail,
        // But my algorithm ensures efficient garbage collection
        while (head!=null) removeFirst();

        size=0;
    }

    @Override
    public E[] toArray() {

        if (head==null) return (E[])new Object[]{}; // Empty array

        Object[] oArray=new Object[size];

        Node<E> node=head;
        for (int i = 0; i < size; i++) {
            if (node==null) throw new Error("Internal error in MyDequeImpl !");

            oArray[i]=node.data;

            node=node.next; // Go to the next node
        }

        return (E[])oArray;
    }

    // Get the size of the list
    @Override
    public int size() {
        return size;
    }

    // Check if the list contains ALL elements of the MyDeque object de
    @Override
    public boolean containsAll(MyDeque<? extends E> de) {
        // Wee need de.toArray(), otherwise cannot access
        // all elements of de without destroying
        // Check every one in turn with this.contains

        for (E e : de.toArray()) if (!contains(e)) return false;

        return true;
    }

    @Override
    public Iterator<E> iterator() {
        return new IteratorImpl<E>();
    }
}
