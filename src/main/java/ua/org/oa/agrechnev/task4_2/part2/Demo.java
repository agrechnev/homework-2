package ua.org.oa.agrechnev.task4_2.part2;


import java.util.Iterator;

/**
 * Created by Oleksiy Grechnyev on 8/23/2016.
 */
public class Demo {
    public static void main(String[] args) {

        // Create a new deque
        MyDeque<String> deque = new MyDequeImpl<>();
        deque.addLast("Orc");
        deque.addLast("Goblin");
        deque.addLast("Elf");
        deque.addLast("Dwarf");
        deque.addLast("Human");
        deque.addLast("Halfling");

        // Demonstrate foreach
        // Iterator should be created automatically
        System.out.println("-------------------");
        System.out.println("Foreach demo:");
        for (String s : deque) System.out.println(s);

        // Demonstrate using iterator explicitly
        Iterator<String> iterator = deque.iterator();

        System.out.println("-------------------");
        System.out.println("While+iterator demo:");
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        System.out.println("-------------------");
        System.out.println("Remove everything demo:");
        // Remove everything as a hard test of remove
        iterator = deque.iterator();
        while (iterator.hasNext()) {
            System.out.println("Removing "+iterator.next());
            iterator.remove();
        }
        System.out.println("Now deque.size()=="+deque.size());

        System.out.println("-------------------");
        System.out.println("Re-initializing deque which is now empty:");
        deque.addLast("Orc");
        deque.addLast("Goblin");
        deque.addLast("Elf");
        deque.addLast("Dwarf");
        deque.addLast("Human");
        deque.addLast("Halfling");

        System.out.println("Selective remove test (Goblin, Human):");
        iterator = deque.iterator();
        while (iterator.hasNext()) { // Selective remove
            String s=iterator.next();
            if (s.equals("Goblin") || s.equals("Human")) iterator.remove();
        }
        System.out.println("Printing the result with foreach:");
        for (String s : deque) System.out.println(s);
        System.out.println("Now deque.size()=="+deque.size());

        System.out.println("-------------------");
        System.out.println("Test of MyDeque<Number> (abstract class):");

        MyDeque<Number> dn=new MyDequeImpl<>();

        dn.addFirst(433);
        dn.addLast(8.88);
        dn.addLast(-200);
        dn.addLast(-100.85);
        dn.addLast(Math.PI);

        for (Number n:dn) System.out.println(n);
        System.out.println("-------------------");
        System.out.println("And with while loop:");

        Iterator<Number> in=dn.iterator();
        while (in.hasNext()) System.out.println(in.next());

    }

}
