package ua.org.oa.agrechnev.task7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by Oleksiy Grechnyev on 9/5/2016.
 * A thread which prints time every second
 * And the main method to test it
 */
public class PrintTime {

    // The thread which sleeps for a while
    // And prints current time approximately every second
    public static void TimeTracker(){
        int oldSec=-100500; // The previous value of second
        final int sleepInterval=20; // For how long to sleep?

        for(;;){ // The infinite loop
            LocalTime lt=LocalTime.now();
            int sec=lt.getSecond();
            if (sec!=oldSec) { // Second has changed
                System.out.println(lt.format(DateTimeFormatter.ofPattern("HH:mm:ss"))); // Print time
                oldSec=sec; // Save the old second
            }

            try {
                Thread.sleep(sleepInterval); // Sleep for while to save CPU time
            } catch (InterruptedException e) {
                // Time to quit
                System.out.println("Bye bye!");
                return;
            }

        }
    }

    public static void main(String[] args) throws IOException {

        // Option 1: override Thread.ru
        Thread thread1=new Thread("Time printer 1"){
            @Override
            public void run() {
                TimeTracker();
            }
        };

        // Option 2: use Runnable
        Thread thread2=new Thread(()->TimeTracker(),"Time printer 2");

        // System in as a proper reader
        BufferedReader in=new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Starting time printer 1.");
        System.out.println("Type anything to stop.");

        thread1.start(); // Start the thread
        String s=in.readLine(); // Read something
        thread1.interrupt(); // Stop the thread

        System.out.println("Starting time printer 2.");
        System.out.println("Type anything to stop.");

        thread2.start(); // Start the thread
        s=in.readLine(); // Read something
        thread2.interrupt(); // Stop the thread

    }
}
