package ua.org.oa.agrechnev.task7;

/**
 * Created by Oleksiy Grechnyev on 9/5/2016.
 * Deadlock demo
 */
public class Deadlock {
    public static void main(String[] args) {
        // Two dummy objects used as locks
        Object lock1=new Object();
        Object lock2=new Object();


        // Adjust these two fields to turn on/off deadlock
        // On my computer:
        // (10,1) = deadlock
        // (1,10) = no deadlock
        // (1,1), (10,10) = indeterminate
        // How long to sleep between requesting locks?
        // Increase to cause deadlock
        final int sleepBetweenLocks=10;
        // How long to sleep between starting threads?
        // Increase to avoid deadlock
        final int sleepBetweenStarts=1;

        // Starting the thread 1
        new Thread(()->{ // Lambda for a Runnable
            System.out.println("Thread 1 start !");
            synchronized (lock1) { // Request lock1, then lock2
                nap(sleepBetweenLocks); // Wait to try to cause deadlock
                synchronized (lock2) {
                    System.out.println("Thread 1: no deadlock !");
                }
            }
        },"Thread 1").start();

        nap(sleepBetweenStarts); // Wait to try to avoid deadlock

        // Starting the thread 2
        new Thread(()->{
            System.out.println("Thread 2 start !");
            synchronized (lock2) { // Reversed order of locks compared to Thread 1
                nap(sleepBetweenLocks); // Wait to try to cause deadlock
                synchronized (lock1) {
                    System.out.println("Thread 2: no deadlock !");
                }
            }
        },"Thread 2").start();
    }

    // Sleep and ignore InterruptedException
    private static void nap(int sleepTime) {
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
