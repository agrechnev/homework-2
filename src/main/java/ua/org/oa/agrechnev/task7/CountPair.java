package ua.org.oa.agrechnev.task7;

/**
 * Created by Oleksiy Grechnyev on 9/5/2016.
 *  A pair of counters with a method to increment and compare them
 *  (synchronized and not-synchronized versions)
 */
public class CountPair {
    private int count1=0;
    private int count2=0;

    // Print both, increment count1, nap for 10 msec, increment count2
    // Non-synchronized version
    public void incNSync(){
        System.out.println(count1+":"+count2);
        count1++;
        nap(10);
        count2++;
    }

    // Synchronization wrapper
    public synchronized void incSync(){
        incNSync();
    }

    // Sleep and ignore InterruptedException
    private static void nap(int sleepTime) {
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
