package ua.org.oa.agrechnev.task7;

/**
 * Created by Oleksiy Grechnyev on 9/5/2016.
 * Demo for the class CountPair
 * Compare Sync vs NSync
 */
public class CountDemo {

    // We have to make it a static field, since lambda expressions cannot use local variables
    // except (effectively) final ones
    private static CountPair countPair;

    public static void main(String[] args) throws InterruptedException {


        System.out.println("Non-synchronized version:");
        countPair = new CountPair();


        // My code block
        Runnable runnable = () -> {
            for (int i = 0; i < 10; i++) { // Run 10 times for each thread
                countPair.incNSync(); // Non-synchronized
            }
        };

        // Create and start 3 threads
        Thread thread1=new Thread(runnable);
        Thread thread2=new Thread(runnable);
        Thread thread3=new Thread(runnable);

        thread1.start();
        thread2.start();
        thread3.start();

        thread1.join(); // Wait for them to complete
        thread2.join();
        thread3.join();

        // Now lets try the synchronized version
        System.out.println("Synchronized version:");

        countPair = new CountPair(); // Reset the counter

        // My code block
        runnable = () -> {
            for (int i = 0; i < 10; i++) { // Run 10 times for each thread
                countPair.incSync(); // Synchronized
            }
        };

        // Create and start 3 threads
        thread1=new Thread(runnable);
        thread2=new Thread(runnable);
        thread3=new Thread(runnable);

        thread1.start();
        thread2.start();
        thread3.start();
    }
}
